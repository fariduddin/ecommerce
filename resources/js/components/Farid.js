import React from "react";
import ReactDOM from "react-dom";

function Farid() {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Farid Component</div>
                        <div className="card-body">I'm an Farid component!</div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Farid;

if (document.getElementById("Farid")) {
    ReactDOM.render(<Farid />, document.getElementById("Farid"));
}
