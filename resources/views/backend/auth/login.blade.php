@extends('backend.auth.master')

@section('sub-content')
  <form class="form-signin" method="POST" action="{{ route('admin.login.submit') }}">
    <h2 class="text-center mb-2">Admin Panel Login</h2>
    @csrf
    @if ( Session::has('login_error') )
      <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! Session::get('login_error') !!}
      </div>
    @endif

    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" id="inputEmail" class="form-control" placeholder="Email address"  name="email" value="{{ old('email') }}" required autofocus>

    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control mt-2" placeholder="Password" name="password" required>

    <div class="checkbox">
      <label>
        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
      </label>
    </div>

    <button class="btn btn-lg btn-primary btn-block" type="submit">Login Now</button> <br />
    <a class="btn text-danger btn-link float-right" href="{{ route('admin.password.request') }}">
      Forgot Your Password?
    </a>
    <div class="clearfix"></div>
  </form>

@endsection
