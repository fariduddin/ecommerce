<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Login Admin Panel | PSTU</title>
  @include('backend.partials.styles')
  <link rel="stylesheet" href="{{ asset('css/login_registration.css') }}">
</head>
<body>
  <div id="app">
    @yield('sub-content')
  </div>

  @include('backend.partials.scripts')
</body>
</html>
