
<script src="{!! asset('js/jquery/jquery-3.3.1.min.js') !!}"></script>
<script src="{!! asset('js/jquery/jquery-easing.min.js') !!}"></script>
<script src="{!! asset('js/modernizr/modernizr.min.js') !!}"></script>


<script src="{!! asset('js/bootstrap/popper.min.js') !!}"></script>
<script src="{!! asset('js/bootstrap/bootstrap.min.js') !!}"></script>

<script src="{!! asset('js/data-table/jquery.dataTable.min.js') !!}"></script>
<script src="{!! asset('js/data-table/bootstrap.dataTable.min.js') !!}"></script>

<script src="{!! asset('js/admin/theme.js') !!}"></script>
<script src="{!! asset('js/admin/main.js') !!}"></script>

<!-- WOW Slider Start-->
<script src="{!! asset('js/wow/wow.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/admin/custom.js') !!}" type="text/javascript"></script>
<script>
new WOW().init();

</script>
