{{-- Noty -> Notification Style  --}}
<link rel="stylesheet" href="{{asset('css/noty/noty.css')}}">
<script src="{!! asset('js/noty/noty.min.js') !!}"></script>

@if ($errors->any())
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
          @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
          @endforeach
        </ul>
      </div>
    </div>
  </div>

@endif

{{-- @if (Session::has('message'))
  <div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {{ Session::get('message') }}
  </div>
@endif --}}

@if (Session::has('message'))
  <script>
  new Noty({
    theme: 'sunset',
    type: 'warning',
    layout: 'topCenter',
    text: "{!! Session::get('message') !!}",
    timeout: 60000
  }).show();
  </script>
@endif

@if (Session::has('success'))
  <script>
  new Noty({
    theme: 'sunset',
    type: 'success',
    layout: 'topCenter',
    text: "{!! Session::get('success') !!}",
    timeout: 2000
  }).show();
  </script>
@endif

@if (Session::has('error'))
  <script>
  new Noty({
    theme: 'sunset',
    type: 'error',
    layout: 'topCenter',
    text: "{!! Session::get('error') !!}",
    timeout: 2000
  }).show();
  </script>
@endif
