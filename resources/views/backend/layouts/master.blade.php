<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>@yield('title', 'Tashfia fashion House')</title>

  <meta name="keywords" content="@yield('keywords', 'PSTU')">
  <meta name="description" content="@yield('description', 'PSTU')">
  <meta name="author" content="Patuakhali Science and Technology University">

  @include('backend.partials.styles')
  @yield('stylesheets')
</head>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <div id="app">
    @include('backend.partials.nav')
    <div class="content-wrapper">
      <div class="container-fluid">

        <div class="mt-6 pl-3">
          @include('backend.partials.messages')
          @yield('content')
        </div>

      </div>
    </div>
    @include('backend.partials.footer')
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
  </div> <!-- End App -->

  @include('backend.partials.scripts')
  @yield('scripts')
</body>
</html>
