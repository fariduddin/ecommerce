@extends('backend.layouts.master')

@section('content')
  <div class="card">
    <div class="card-header">
      Add Color
    </div>
    <div class="card-body">
      <form action="{{ route('admin.color.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="name">Color Name</label>
          <input type="text" class="form-control" name="name" id="name" aria-describedby="emailHelp" placeholder="Enter Color Name" required>
        </div>

        <div class="form-group">
          <label for="code">Color Code</label>
          <input type="color" class="form-control" name="code" id="code" aria-describedby="emailHelp" placeholder="Enter Color Code" required style="padding: 0px">
        </div>

        <button type="submit" class="btn btn-primary">Add Color</button>
      </form>
    </div>
  </div>
@endsection
