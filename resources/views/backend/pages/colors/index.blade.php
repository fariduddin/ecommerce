@extends('backend.layouts.master')

@section('content')
  <div class="card">
    <div class="card-header">
      <div class="float-left">Manage Colors</div>
      <div class="float-right"><a href="{{ route('admin.color.create') }}" class="btn btn-primary">Create a new color</a></div>
      <div class="clearfix"></div>
    </div>
    <div class="card-body">
      <table class="table table-hover table-striped" id="dataTable">
        <thead>
          <tr>
            <th>#</th>
            <th>Color Name</th>
            <th>Color Code</th>
            <th>Action</th>
          </tr>
        </thead>

        @foreach ($colors as $color)
          <tr>
            <td>{{ $loop->index+1 }}</td>
            <td>{{ $color->name }}</td>
            <td>{{ $color->code }}</td>

            <td>
              <a href="{{ route('admin.color.edit', $color->id) }}" class="btn btn-success">Edit</a>

              <a href="#deleteModal{{ $color->id }}" data-toggle="modal" class="btn btn-danger">Delete</a>

              <!-- Delete Modal -->
              <div class="modal fade" id="deleteModal{{ $color->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Are sure to delete?</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form action="{!! route('admin.color.delete', $color->id) !!}"  method="post">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger">Permanent Delete</button>
                      </form>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>

            </td>
          </tr>
        @endforeach

      </table>
    </div>
  </div>
@endsection
