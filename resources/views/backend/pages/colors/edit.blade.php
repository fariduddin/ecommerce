@extends('backend.layouts.master')

@section('content')
  <div class="card">
    <div class="card-header">
      Edit Color
    </div>
    <div class="card-body">
      <form action="{{ route('admin.color.update', $color->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="name">Color Name</label>
          <input type="text" class="form-control" name="name" id="name" aria-describedby="emailHelp" value="{{ $color->name }}">
        </div>
        <div class="form-group">
          <label for="code">Color Code</label>
          <p><strong>Previous Code: </strong> {{ $color->code }}</p>
          <input type="color" class="form-control" name="code" id="code" aria-describedby="emailHelp" value="{{ $color->code }}" style="padding: 0px">
        </div>

        <button type="submit" class="btn btn-success">Update Color</button>
      </form>
    </div>
  </div>
@endsection
