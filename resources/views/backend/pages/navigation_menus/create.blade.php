@extends('backend.layouts.master')

@section('content')
  <div class="card">
    <div class="card-header">
      Add Navigation menu
    </div>
    <div class="card-body">
      <form action="{{ route('admin.navigation_menu.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="title">Navigation Title</label>
          <input type="text" class="form-control" name="title" id="title" aria-describedby="emailHelp" placeholder="Enter Navigation Menu Title">
        </div>
        <div class="form-group">
          <label for="link">Navigation Link (If this is only a menu with clickable link)</label>
          <input type="text" class="form-control" name="link" id="link" aria-describedby="emailHelp" placeholder="Enter Navigation Menu Link">
        </div>

        <button type="submit" class="btn btn-primary">Add Navigation Menu</button>
      </form>
    </div>
  </div>
@endsection
