@extends('backend.layouts.master')

@section('content')
  <div class="card">
    <div class="card-header">
      <div class="float-left">Manage Navigation Menus</div>
      <div class="float-right"><a href="{{ route('admin.navigation_menu.create') }}" class="btn btn-primary">Create a new navigation_menu</a></div>
      <div class="clearfix"></div>
    </div>
    <div class="card-body">
      <table class="table table-hover table-striped" id="dataTable">
        <thead>
          <tr>
            <th>#</th>
            <th>Navigation Menu Title</th>
            <th>Navigation Menu Link</th>
            <th>Action</th>
          </tr>
        </thead>

        @foreach ($navigation_menus as $navigation_menu)
          <tr>
            <td>{{ $loop->index+1 }}</td>
            <td>{{ $navigation_menu->title }}</td>
            <td>{{ $navigation_menu->link }}</td>

            <td>
              <a href="{{ route('admin.navigation_menu.edit', $navigation_menu->id) }}" class="btn btn-success">Edit</a>

              <a href="#deleteModal{{ $navigation_menu->id }}" data-toggle="modal" class="btn btn-danger">Delete</a>

              <!-- Delete Modal -->
              <div class="modal fade" id="deleteModal{{ $navigation_menu->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Are sure to delete?</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form action="{!! route('admin.navigation_menu.delete', $navigation_menu->id) !!}"  method="post">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger">Permanent Delete</button>
                      </form>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>

            </td>
          </tr>
        @endforeach

      </table>
    </div>
  </div>
@endsection
