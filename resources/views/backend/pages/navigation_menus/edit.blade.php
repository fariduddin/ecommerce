@extends('backend.layouts.master')

@section('content')
  <div class="card">
    <div class="card-header">
      Edit Navigation Menu
    </div>
    <div class="card-body">
      <form action="{{ route('admin.navigation_menu.update', $navigation_menu->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="title">Menu title</label>
          <input type="text" class="form-control" name="title" id="title" aria-describedby="emailHelp" value="{{ $navigation_menu->title }}">
        </div>

        <div class="form-group">
          <label for="link">Navigation Menu (optional)</label>
          <input type="url" class="form-control" name="link" id="link" aria-describedby="emailHelp" value="{{ $navigation_menu->link }}">
        </div>

        <button type="submit" class="btn btn-success">Update Navigation Menu</button>
      </form>
    </div>
  </div>
@endsection
