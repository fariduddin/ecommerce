@extends('backend.layouts.master')

@section('content')
  <div class="card mb-3">
    <div class="card-header">
      <i class="fa fa-table"></i> Sliders Of Priyo Shop
      <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#addModal"><i class="fa fa-fw fa-plus"></i>Add Slider</button>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Title</th>
              <th>Image</th>
              <th>Priority</th>
              <th>Manage</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Title</th>
              <th>Image</th>
              <th>Priority</th>
              <th>Manage</th>
            </tr>
          </tfoot>
          <tbody>
            @foreach($sliders as $slider)
              <tr>
                <td>{{ $slider->text }}</td>
                <td><a href="{{ asset('images/slider/'.$slider->image) }}" target="_blank"><img src="{{ asset('images/slider/'.$slider->image) }}" alt="" height="50px" width="50px"></a></td>
                <td>{{ $slider->priority }}</td>
                <td>
                  <button class="btn btn-outline-primary" data-toggle="modal" data-target="#editGallery{{ $slider->id }}"><i class="fa fa-edit"></i>Edit</button>

                  {{-- Edit Modal --}}
                  <div class="modal fade" id="editGallery{{ $slider->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="addForm">Edit Slider</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <form class="form-control" action="{{ route('admin.sliders.update', $slider->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                              <label for="text" class="col-form-label">Title <span class="text-danger"><b>*</b></span></label>
                              <input type="text" class="form-control" name="text" id="text" value="{{ $slider->text }}">
                            </div>

                            <div class="form-group">
                              <label for="image" class="col-form-label">Image </label>
                              <input type="file" class="form-control" name="image" id="image">
                            </div>

                            <div class="form-group">
                              <label for="button_link" class="col-form-label">Button Link <span class="text-danger"><b>(if you want to go button to other page)</b></span></label>
                              <input type="url" class="form-control" name="button_link" id="button_link" value="{{ $slider->button_link }}">
                            </div>
                            <div class="form-group">
                              <label for="priority" class="col-form-label">Priority <span class="text-danger"><b>(lower value will get higher priority)</b></span></label>
                              <input type="number" class="form-control" name="priority" id="priority" value="{{ $slider->priority != NULL ? $slider->priority : 10 }}">
                            </div>
                            <button type="submit" class="btn btn-primary float-right mt-3">Save Information</button>
                            <button type="button" class="btn btn-secondary float-right mt-3 mr-2" data-dismiss="modal">Close</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  {{-- End Edit Modal --}}

                  <button type="submit" data-toggle="modal" data-target="#deleteModal{{ $slider->id }}" class="btn btn-outline-danger"><i class="fa fa-trash"></i>Delete</button>

                  {{-- Delete Modal --}}
                  <div class="modal fade" id="deleteModal{{ $slider->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="deleteModalLabel">Are you want to delete this data?</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-footer">
                          <form class="" action="{{ route('admin.sliders.delete', $slider->id) }}" method="post">
                            @csrf
                            <button type="submit" class="btn btn-outline-danger"><i class="fa fa-trash"></i> Delete</button>
                          </form>
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>


    {{-- Add Modal --}}
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="addForm">Slider Information</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-control" id="needs-validation" action="{{ route('admin.sliders.store') }}" method="POST" enctype="multipart/form-data" novalidate>
              @csrf
              <div class="form-group">
                <label for="text" class="col-form-label">Title <span class="text-danger"><b>*</b></span></label>
                <input type="text" class="form-control" name="text" id="text" placeholder="Slider Text" required>
                <div class="invalid-feedback">
                  Please give a title.
                </div>
              </div>

              <div class="form-group">
                <label for="image" class="col-form-label">Image <span class="text-danger"><b>*</b></span></label>
                <input type="file" class="form-control" name="image" id="image" required>
                <div class="invalid-feedback">
                  Please give an image.
                </div>
              </div>

              <div class="form-group">
                <label for="button_link" class="col-form-label">Button Link <span class="text-danger"><b>(if you want to go button to other page)</b></span></label>
                <input type="url" class="form-control" name="button_link" id="button_link">
                <div class="invalid-feedback">
                  Please give a valid URL for the slider
                </div>
              </div>

              <div class="form-group">
                <label for="priority" class="col-form-label">Priority <span class="text-danger"><b>lower value will get higher priority)</b></span></label>
                <input type="number" class="form-control" name="priority" id="priority" placeholder="Slider Priority" value="10">
              </div>

              <button type="submit" class="btn btn-primary float-right mt-3">Save Information</button>
              <button type="button" class="btn btn-secondary float-right mt-3 mr-2" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>

  @endsection
