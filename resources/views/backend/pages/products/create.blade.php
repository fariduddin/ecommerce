@extends('backend.layouts.master')

@section('stylesheets')
  <script src="{{ asset('js/tinymce/tiny_old.min.js') }}"></script>
  <script>
  // tinymce.init({ selector:'#description' });
  tinymce.init({
    selector: '#description, #washcare',
    theme: 'modern',
    plugins: 'print preview code fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount   imagetools   contextmenu colorpicker textpattern help',
    toolbar1: 'formatselect | code bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
    image_advtab: true,
    templates: [
      { title: 'Test template 1', content: 'Test 1' },
      { title: 'Test template 2', content: 'Test 2' }
    ],
    content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css'
    ]
  });
  </script>
  <link rel="stylesheet" href="{{ asset('css/select2/select2.min.css') }}" />
@endsection

@section('content')
  <div class="card">
    <div class="card-header">
      Add Product
    </div>
    <div class="card-body">
      <form action="{{ route('admin.product.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" class="form-control" name="title" id="title" aria-describedby="title" placeholder="Enter product title">
        </div>
        <div class="form-group">
          <label for="description">Description</label>
          <textarea name="description" rows="8" cols="80" class="form-control" id="description"></textarea>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Price</label>
          <input type="number" class="form-control" name="price" min="0" id="exampleInputEmail1">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Quantity</label>
          <input type="number" class="form-control" name="quantity" min="0" id="exampleInputEmail1">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Select Category</label>
          <select class="form-control" name="category_id">
            <option value="">Please select a category for the product</option>
            @foreach (App\Models\Category::orderBy('priority', 'asc')->where('parent_id', NULL)->get() as $parent)
              <option value="{{ $parent->id }}">{{ $parent->name }}</option>
              @if ($parent->is_gender_wise)

                @foreach (App\Models\Category::where('parent_id', $parent->id)->orderBy('priority', 'asc')->where('gender', 1)->get() as $child)
                  <option value="{{ $child->id }}">&nbsp;&nbsp;&nbsp;&nbsp;Boys --> {{ $child->name }}</option>
                @endforeach

                @foreach (App\Models\Category::where('parent_id', $parent->id)->orderBy('priority', 'asc')->where('gender', 0)->get() as $child)
                  <option value="{{ $child->id }}">&nbsp;&nbsp;&nbsp;&nbsp;Girls --> {{ $child->name }}</option>
                @endforeach

              @else
                @foreach (App\Models\Category::orderBy('name', 'asc')->where('parent_id', $parent->id)->get() as $child)
                  <option value="{{ $child->id }}"> &nbsp;&nbsp;&nbsp;&nbsp;{{ $child->name }}</option>
                @endforeach
              @endif
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Select Brand</label>
          <select class="form-control" name="brand_id">
            <option value="">Please select a brand for the product</option>
            @foreach (App\Models\Brand::orderBy('name', 'asc')->get() as $brand)
              <option value="{{ $brand->id }}" {{ $brand->slug == 'others'? 'selected': '' }}>{{ $brand->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="product_image">Product Image</label>

          <div class="row">
            <div class="col-md-3">
              <input type="file" class="form-control" name="product_image[]" id="product_image" >
            </div>
            <div class="col-md-3">
              <input type="file" class="form-control" name="product_image[]" id="product_image" >
            </div>
            <div class="col-md-3">
              <input type="file" class="form-control" name="product_image[]" id="product_image" >
            </div>
            <div class="col-md-3">
              <input type="file" class="form-control" name="product_image[]" id="product_image" >
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="product_colors">Product Colors</label>

          <select class="select2-multi" name="product_colors[]" id="product_colors" multiple style="min-width: 250px">
            @foreach (App\Models\Color::all() as $color)
              <option value="{{ $color->id }}">
                {{ $color->name }}
              </option>
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <label for="product_sizes">Product Sizes</label>
          <div class="row">
            <div class="col-md-3">
              <input type="text" class="form-control" name="product_sizes[]" id="product_sizes" >
            </div>
            <div class="col-md-3">
              <input type="text" class="form-control" name="product_sizes[]" id="product_sizes" >
            </div>
            <div class="col-md-3">
              <input type="text" class="form-control" name="product_sizes[]" id="product_sizes" >
            </div>
            <div class="col-md-3">
              <input type="text" class="form-control" name="product_sizes[]" id="product_sizes" >
            </div>
          </div>

        </div>

        <hr>

        <h3>More Options (Optional)</h3>

        <div class="card card-body p-2">

          <div class="form-group">
            <label for="offer_discount">Product Discount Offer</label>
            <input type="number" class="form-control" name="offer_discount" id="offer_discount">
          </div>

          <div class="form-group">
            <label for="offer_last_date">Product Discount Offer Last Date</label>
            <input type="date" class="form-control" name="offer_last_date" id="offer_last_date">
          </div>

          <div class="form-group">
            <label for="sku">SKU</label>
            <input type="text" class="form-control" name="sku" id="sku">
          </div>

          <div class="form-group">
            <label for="fabrics">Fabrics</label>
            <input type="text" class="form-control" name="fabrics" id="fabrics">
          </div>

          <div class="form-group">
            <label for="cut">Cut</label>
            <input type="text" class="form-control" name="cut" id="cut">
          </div>

          <div class="form-group">
            <label for="sleeve">Sleeve</label>
            <input type="text" class="form-control" name="sleeve" id="sleeve">
          </div>

          <div class="form-group">
            <label for="collar">Collar</label>
            <input type="text" class="form-control" name="collar" id="collar">
          </div>

          <div class="form-group">
            <label for="length">Length</label>
            <input type="text" class="form-control" name="length" id="length">
          </div>

          <div class="form-group">
            <label for="occasion">Occation</label>
            <input type="text" class="form-control" name="occasion" id="occasion">
          </div>

          <div class="form-group">
            <label for="event">Event</label>
            <input type="text" class="form-control" name="event" id="event">
          </div>

          <div class="form-group">
            <label for="value_addition">Value Addition</label>
            <input type="text" class="form-control" name="value_addition" id="value_addition">
          </div>
          <div class="form-group">
          <label for="washcare">Wash Care</label>
          <textarea name="washcare" rows="4" cols="80" class="form-control" id="washcare"></textarea>
        </div>

          <div class="form-group">
            <label for="meta_keywords">Meta Keywords (use comma after a keyword)</label>
            <textarea rows="3" class="form-control" name="meta_keywords" id="meta_keywords"></textarea>
          </div>

          <div class="form-group">
            <label for="meta_description">Meta Description Use description for search engine</label>
            <textarea rows="3" class="form-control" name="meta_description" id="meta_description"></textarea>
          </div>


        </div>

        <button type="submit" class="btn btn-primary mt-3"><i class="fa fa-check"></i> <i class="fa fa-check"></i> Add Product</button>
      </form>
    </div>
  </div>

@endsection

@section('scripts')
  <script src="{{ asset('js/select2/select2.min.js') }}"></script>
  <script type="text/javascript">
  $('.select2-multi').select2();
  </script>
@endsection
