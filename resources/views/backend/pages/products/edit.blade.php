@extends('backend.layouts.master')

@section('stylesheets')
  <script src="{{ asset('js/tinymce/tiny_old.min.js') }}"></script>
  <script>
  tinymce.init({
    selector: '#description, #washcare',
    theme: 'modern',
    plugins: 'print preview code fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount   imagetools   contextmenu colorpicker textpattern help',
    toolbar1: 'formatselect | code bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
    image_advtab: true,
    templates: [
      { title: 'Test template 1', content: 'Test 1' },
      { title: 'Test template 2', content: 'Test 2' }
    ],
    content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css'
    ]
  });
  </script>
  <link rel="stylesheet" href="{{ asset('css/select2/select2.min.css') }}" />
@endsection

@section('content')
  <div class="card">
    <div class="card-header">
      Edit Product
    </div>
    <div class="card-body">
      <form action="{{ route('admin.product.update', $product->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="name">Title</label>
          <input type="text" class="form-control" name="title" id="name" aria-describedby="emailHelp" value="{{ $product->title }}" >
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Description</label>
          <textarea name="description" id="description" rows="8" cols="80" class="form-control">{!! $product->description !!}</textarea>

        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Price</label>
          <input type="number" class="form-control" name="price" id="exampleInputEmail1"  min="0" value="{{ $product->price }}">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Quantity</label>
          <input type="number" class="form-control" name="quantity" id="exampleInputEmail1"  min="0" value="{{ $product->quantity }}">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Select Category</label>
          <select class="form-control" name="category_id">
            <option value="">Please select a category for the product</option>
            @foreach (App\Models\Category::orderBy('priority', 'asc')->where('parent_id', NULL)->get() as $parent)
              <option value="{{ $parent->id }}">{{ $parent->name }}</option>
              @if ($parent->is_gender_wise)

                @foreach (App\Models\Category::where('parent_id', $parent->id)->orderBy('priority', 'asc')->where('gender', 1)->get() as $child)
                  <option value="{{ $child->id }}" {{ $child->id == $product->category_id ? 'selected': '' }}>&nbsp;&nbsp;&nbsp;&nbsp;Boys --> {{ $child->name }}</option>
                @endforeach

                @foreach (App\Models\Category::where('parent_id', $parent->id)->orderBy('priority', 'asc')->where('gender', 0)->get() as $child)
                  <option value="{{ $child->id }}" {{ $child->id == $product->category_id ? 'selected': '' }}>&nbsp;&nbsp;&nbsp;&nbsp;Girls --> {{ $child->name }}</option>
                @endforeach

              @else
                @foreach (App\Models\Category::orderBy('name', 'asc')->where('parent_id', $parent->id)->get() as $child)
                  <option value="{{ $child->id }}" {{ $child->id == $product->category_id ? 'selected': '' }}> &nbsp;&nbsp;&nbsp;&nbsp;{{ $child->name }}</option>
                @endforeach
              @endif
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Select Brand</label>
          <select class="form-control" name="brand_id">
            <option value="">Please select a brand for the product</option>
            @foreach (App\Models\Brand::orderBy('name', 'asc')->get() as $br)
              <option value="{{ $br->id }}" {{ $br->id == $product->brand->id ? 'selected' : '' }}>{{ $br->name }}</option>
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <label for="product_image">Product Image <a href="#modalOldImages" class="btn btn-success btn-sm" data-toggle="modal"><i class="fa fa-edit"></i> Edit Old Images</a></label>



          <div class="row">
            @php
            $remaining_image = 4-$product->images()->count();
            @endphp
            @for ($i=1; $i <= $remaining_image; $i++)
              <div class="col-md-3">
                <input type="file" class="form-control" name="product_image[]" id="product_image" >
              </div>
            @endfor

          </div>
        </div>

        <div class="form-group">
          <label for="product_colors">Product Colors</label>

          <select class="select2-multi" name="product_colors[]" id="product_colors" multiple style="min-width: 250px">
            @foreach (App\Models\Color::all() as $color)
              <option value="{{ $color->id }}" {{ $product->hasColor($color->id) ? 'selected' : '' }}>
                {{ $color->name }}
              </option>
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <label for="product_sizes">Product Sizes</label>
          <div class="row">

            @if ($product->sizes()->count() > 0)
              @foreach ($product->sizes as $size)
                <div class="col-md-3">
                  <input type="text" class="form-control" name="product_sizes[]" value="{{ $size->size }}" id="product_sizes" >
                </div>
              @endforeach
            @endif
            @php
            $remaining_size = 4-$product->sizes()->count();
            @endphp
            @for ($i=1; $i <= $remaining_size; $i++)
              <div class="col-md-3">
                <input type="text" class="form-control" name="product_sizes[]" id="product_sizes" >
              </div>
            @endfor

          </div>

        </div>

        <hr>

        <h3>More Options (Optional)</h3>

        <div class="card card-body p-2">

          <div class="form-group">
            <label for="offer_discount">Product Discount Offer</label>
            <input type="number" class="form-control" name="offer_discount" value="{{ $product->offer_discount }}" id="offer_discount">
          </div>

          <div class="form-group">
            <label for="offer_last_date">Product Discount Offer Last Date</label>
            <br>
            <p><strong>Previous Date: </strong> {{ $product->offer_last_date }} </p>
            <input type="date" class="form-control" name="offer_last_date" value="{{ $product->offer_last_date }}" id="offer_last_date">
          </div>

          <div class="form-group">
            <label for="sku">SKU</label>
            <input type="text" class="form-control" name="sku" value="{{ $product->sku }}" id="sku">
          </div>

          <div class="form-group">
            <label for="fabrics">Fabrics</label>
            <input type="text" class="form-control" name="fabrics" value="{{ $product->fabrics }}" id="fabrics">
          </div>

          <div class="form-group">
            <label for="cut">Cut</label>
            <input type="text" class="form-control" name="cut" value="{{ $product->cut }}" id="cut">
          </div>

          <div class="form-group">
            <label for="sleeve">Sleeve</label>
            <input type="text" class="form-control" name="sleeve" value="{{ $product->sleeve }}" id="sleeve">
          </div>

          <div class="form-group">
            <label for="collar">Collar</label>
            <input type="text" class="form-control" name="collar" value="{{ $product->collar }}" id="collar">
          </div>

          <div class="form-group">
            <label for="length">Length</label>
            <input type="text" class="form-control" name="length" value="{{ $product->length }}" id="length">
          </div>

          <div class="form-group">
            <label for="occasion">Occation</label>
            <input type="text" class="form-control" name="occasion" value="{{ $product->occasion }}" id="occasion">
          </div>

          <div class="form-group">
            <label for="event">Event</label>
            <input type="text" class="form-control" name="event" value="{{ $product->event }}" id="event">
          </div>

          <div class="form-group">
            <label for="value_addition">Value Addition</label>
            <input type="text" class="form-control" name="value_addition" value="{{ $product->value_addition }}" id="value_addition">
          </div>
          
          <div class="form-group">
              <label for="washcare">Wash Care</label>
              <textarea name="washcare" rows="4" cols="80" class="form-control" id="washcare">{!! $product->washcare !!}</textarea>
          </div>

          <div class="form-group">
            <label for="meta_keywords">Meta Keywords (use comma after a keyword)</label>
            <textarea rows="3" class="form-control" name="meta_keywords" id="meta_keywords">{{ $product->meta_keywords }}</textarea>
          </div>

          <div class="form-group">
            <label for="meta_description">Meta Description Use description for search engine</label>
            <textarea rows="3" class="form-control" name="meta_description" id="meta_description">{{ $product->meta_description }}</textarea>
          </div>


        </div>

        <button type="submit" class="btn btn-primary">Update Product</button>
      </form>


      <!-- Edit Images Modal -->
      <div class="modal fade" id="modalOldImages" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Old Images For Product - {{ $product->title }}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              @foreach ($product->images as $image)
                <form action="{{ route('admin.product.image.delete', $product->id) }}" method="post">
                  @csrf
                  <img src="{{ asset('images/products/'.$image->image) }}" width="200">
                  <input type="hidden" name="image_id" value="{{ $image->id }}">
                  <input type="submit" class="btn btn-danger btn-sm" value="Delete Image">
                </form>
              @endforeach
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </div>
      </div>



    </div>
  </div>
@endsection


@section('scripts')
  <script src="{{ asset('js/select2/select2.min.js') }}"></script>
  <script type="text/javascript">
  $('.select2-multi').select2();
  </script>
@endsection
