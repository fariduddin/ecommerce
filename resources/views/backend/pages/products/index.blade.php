@extends('backend.layouts.master')

@section('content')
  <div class="card">
    <div class="card-header">
      <div class="float-left">Manage Products</div>
      <div class="float-right"><a href="{{ route('admin.product.create') }}" class="btn btn-primary">Create a new product</a></div>
      <div class="clearfix"></div>
    </div>
    <div class="card-body">
      <table class="table table-hover table-striped" id="dataTable">
        <thead>
          <tr>
            <th>#</th>
            <th>Product Title</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Action</th>
          </tr>
        </thead>

        @foreach ($products as $product)
          <tr>
            <td>{{ $loop->index+1 }}</td>
            <td>{{ $product->title }}</td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->quantity }}</td>
            <td>
              <form action="{{ route('admin.product.addFeatured', $product->id) }}" style="display: inline!important" method="post">
                @csrf
                <button type="submit" class="btn btn-danger"><i class="fa fa-heart"></i> </button>
              </form>
              <a href="{{ route('admin.product.edit', $product->id) }}" class="btn btn-success">Edit</a>
              <a href="#deleteModal{{ $product->id }}" data-toggle="modal" class="btn btn-danger">Delete</a>

              <!-- Delete Modal -->
              <div class="modal fade" id="deleteModal{{ $product->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Are sure to delete?</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form action="{!! route('admin.product.delete', $product->id) !!}"  method="post">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger">Permanent Delete</button>
                      </form>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>

            </td>
          </tr>
        @endforeach

      </table>
    </div>
  </div>
@endsection
