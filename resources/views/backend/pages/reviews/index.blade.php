@extends('backend.layouts.master')

@section('content')

  <div class="card card-body">

    <h3>Manage Reviews</h3>
    <hr>
    <div class="row">
      @if ($reviews->count() > 0)
        <table class="table table-bordered table-striped" id="dataTable">
          <tr>
            <th width="5%">No.</th>
            <th width="15%">Product</th>
            <th width="15%">Rating</th>
            <th width="20%">Message</th>
            <th width="10%">Status</th>
            <th width="30%">Action</th>
          </tr>
          @foreach ($reviews as $review)
            <tr>
              <td>{{ $loop->index+1 }}</td>
              <td>
                <a href="{{ route('products.show', $review->product->slug) }}" target="_blank">{{ $review->product->title }}</a>
              </td>
              <td>
                @for ($rating=1; $rating <= $review->rating; $rating++)
                  <i class="fa fa-star"></i>
                @endfor
              </td>
              <td>{{ $review->message }}</td>
              <td>{{ ($review->status == 1) ? 'published' : 'hidden' }}</td>
              <td>
                <form style="display: inline!important" action="{{ route('admin.reviews.delete', $review->id) }}" method="post">
                  @csrf
                  <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                </form>
                <form style="display: inline!important" action="{{ route('admin.reviews.change_status', $review->id) }}" method="post">
                  @csrf
                  <button type="submit" class="btn btn-info"><i class="fa fa-check"></i>
                    {{ $review->status == 1 ? 'Hide Now' : 'Publish Now' }}
                  </button>
                </form>
              </td>
            </tr>

          @endforeach
        </table>
      @else
        <div class="container">
          <p class="text-danger">No reviews submitted yet by anyone !!</p>
        </div>
      @endif
    </div>

  </div>

@endsection
