@extends('backend.layouts.master')

@section('content')
  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="{!! route('admin.dashboard') !!}">Dashboard</a>
    </li>
    <li class="breadcrumb-item active"> Account</li>
  </ol>

  <div class="card mb-3">
    <div class="card-header">
      <i class="fa fa-table mr-2"></i> Change Password
    </div>
  </div>

<div class="admin-page">
<div class="row">
  <div class="col-md-6 offset-md-3">
    <form class="form-control" action="{{ route('admin.changePassword') }}" method="POST">
      @csrf
      <div class="form-group">
        <label for="old_password">Old Password</label>
        <input type="password" class="form-control" name="old_password" id="old_password" placeholder="Old Password">
        @if ($errors->has('old_password'))
          <span class="help-block">
            <strong class="text-danger">{{ $errors->first('old_password') }}</strong>
          </span>
        @endif
        @if(Session::has('old_password'))
          <span class="help-block">
            <strong class="text-danger">{{ Session::get('old_password') }}</strong>
          </span>
        @endif
      </div>
      <div class="form-group">
        <label for="new_password">New Password</label>
        <input type="password" class="form-control" name="new_password" id="new_password" placeholder="New Password">
        @if ($errors->has('new_password'))
          <span class="help-block">
            <strong class="text-danger">{{ $errors->first('new_password') }}</strong>
          </span>
        @endif
        @if(Session::has('new_password'))
          <span class="help-block">
            <strong class="text-danger">{{ Session::get('new_password') }}</strong>
          </span>
        @endif
      </div>
      <div class="form-group">
        <label for="confirm_new_password">Confirm New Password</label>
        <input type="password" class="form-control" name="confirm_new_password" id="confirm_new_password" placeholder="Confirm New Password">
        @if ($errors->has('confirm_password'))
          <span class="help-block">
            <strong class="text-danger">{{ $errors->first('confirm_password') }}</strong>
          </span>
        @endif
        @if(Session::has('confirm_password'))
          <span class="help-block">
            <strong class="text-danger">{{ Session::get('confirm_password') }}</strong>
          </span>
        @endif
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block mt-3 mb-3 float-right">Submit</button>
      </div>
    </form>
  </div>
</div>
</div>
@endsection
