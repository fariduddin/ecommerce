@extends('backend.layouts.master')

@section('stylesheets')
	<style>
		.bg-dashboard-link {
		    background: #dbdfe3;
		    padding: 30px;
		    margin: 20px;
		    border: 1px solid #dbdfe3;
		    cursor: pointer;
		    transition: .3s;
		    min-height: 150px;
		}
		.bg-dashboard-link:hover {
		    background: #00425d30;
		}
		
	</style>
@endsection

@section('content')
  <!-- Breadcrumbs-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="{!! route('admin.dashboard') !!}">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">My Dashboard</li>
  </ol>

  <div class="card card-body">
    <h2>Welcome to Dashboad Panel</h2>
    <div class="row mt-2">
    	<div class="col-md-3 pointer bg-dashboard-link" onclick="location.href='{{ route('admin.orders.index') }}'">
    		 <h3>Manage Orders</h3>
    		 <span class="badge badge-danger">{{ App\Models\Order::where('is_seen_by_admin', 0)->get()->count() }}</span> Unseen Orders <br />
    		 <span class="badge badge-info">{{ App\Models\Order::all()->count() }}</span> Total Orders <br />
    	</div>
    	<div class="col-md-3 pointer bg-dashboard-link" onclick="location.href='{{ route('admin.products') }}'">
    		 <h3>Manage Products</h3>
    		 <span class="badge badge-info">{{ App\Models\Product::all()->count() }}</span> Total Products <br />
    	</div>
    	
    	<div class="col-md-3 pointer bg-dashboard-link" onclick="location.href='{{ route('admin.careers.index') }}'">
    		 <h3>Manage Careers</h3>
    		 <span class="badge badge-info">{{ App\Models\Career::where('status', 0)->get()->count() }}</span> Unseen Career
    	</div>
    	
    	<div class="col-md-3 pointer bg-dashboard-link" onclick="location.href='{{ route('admin.pages') }}'">
    		 <h3>Manage Pages</h3>
    		 <span class="badge badge-info">{{ App\Models\Page::all()->count() }}</span> Total Pages
    	</div>
    	
    	<div class="col-md-3 pointer bg-dashboard-link" onclick="location.href='{{ route('admin.settings.index') }}'">
    		 <h3>Manage Settings</h3>
    		 <i class="fa fa-cog"></i> Change Settings
    	</div>
    </div>
  </div>

@endsection
