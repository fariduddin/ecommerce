@extends('backend.layouts.master')

@section('content')
  <div class="card">
    <div class="card-header">
      Add Payment
    </div>
    <div class="card-body">
      <form action="{{ route('admin.payment.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" name="name" id="name" aria-describedby="emailHelp" placeholder="Enter Payment Name">
        </div>

        <div class="form-group">
          <label for="short_name">Short Name (Don't change it)</label>
          <input type="text" class="form-control" name="short_name" id="short_name" aria-describedby="emailHelp" placeholder="Enter Payment Name">
        </div>

        <div class="form-group">
          <label for="no">Payment No</label>
          <input type="text" class="form-control" name="no" id="no" aria-describedby="emailHelp" placeholder="Enter Payment no like 01711121212">
        </div>

        <div class="form-group">
          <label for="type">Payment Type</label>
          <input type="text" class="form-control" name="type" id="type" aria-describedby="emailHelp" placeholder="Enter Payment type like agent/personal">
        </div>

        <div class="form-group">
          <label for="priority">Payment priority</label>
          <input type="number" class="form-control" name="priority" id="priority" aria-describedby="emailHelp" placeholder="Enter Payment priority like 1,2,3">
        </div>

        <div class="form-group">
          <label for="image">Payment image</label>
          <input type="file" class="form-control" name="image" id="image" >
        </div>

        <button type="submit" class="btn btn-primary">Add Payment</button>
      </form>
    </div>
  </div>
@endsection
