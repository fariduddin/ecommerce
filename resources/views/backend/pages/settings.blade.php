@extends('backend.layouts.master')

@section('content')
  <!-- Breadcrumbs-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="{!! route('admin.dashboard') !!}">Admin Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Site and Other Settings</li>
  </ol>

  <div class="card">
    <div class="card-header">
      Edit Settings
    </div>
    <div class="card-body">

      <form action="{{ route('admin.settings.update') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="site_title">Site Title (* Required)</label>
          <input type="text" class="form-control" name="site_title" id="site_title" aria-describedby="emailHelp" value="{{ $setting->site_title }}" placeholder="Enter Site Title">
        </div>

        <div class="form-group">
          <label for="site_location">Site location</label>
          <input type="text" class="form-control" name="site_location" id="site_location" aria-describedby="emailHelp" value="{{ $setting->site_location }}" placeholder="Enter site location">
        </div>

        <div class="form-group">
          <label for="phone_no">Site Phone No</label>
          <input type="text" class="form-control" name="phone_no" id="phone_no" aria-describedby="emailHelp" value="{{ $setting->phone_no }}" placeholder="Enter site location">
        </div>

        <div class="form-group">
          <label for="email">Site Email</label>
          <input type="text" class="form-control" name="email" id="email" aria-describedby="emailHelp" value="{{ $setting->email }}" placeholder="Enter site email address">
        </div>

        <div class="form-group">
          <label for="shipping_cost_inside_dhaka">Shipping Cost inside Dhaka</label>
          <input type="text" class="form-control" name="shipping_cost_inside_dhaka" id="shipping_cost_inside_dhaka" aria-describedby="emailHelp" value="{{ $setting->shipping_cost_inside_dhaka }}" placeholder="Enter shipping cost inside dhaka">
        </div>

        <div class="form-group">
          <label for="shipping_cost_outside_dhaka">Shipping Cost outside Dhaka</label>
          <input type="text" class="form-control" name="shipping_cost_outside_dhaka" id="shipping_cost_outside_dhaka" aria-describedby="emailHelp" value="{{ $setting->shipping_cost_outside_dhaka }}" placeholder="Enter shipping cost outside dhaka">
        </div>

        <div class="form-group">
          <label for="site_meta_keywords">Site Meta Keywords</label>
          <textarea class="form-control" name="site_meta_keywords" id="site_meta_keywords" aria-describedby="emailHelp" value="{{ $setting->site_meta_keywords }}">{{ $setting->site_meta_keywords }}</textarea>
        </div>

        <div class="form-group">
          <label for="site_meta_description">Site Meta Descriptions</label>
          <textarea class="form-control" name="site_meta_description" id="site_meta_description" aria-describedby="emailHelp" value="{{ $setting->site_meta_description }}">{{ $setting->site_meta_description }}</textarea>
        </div>

        <hr>
        <h3>Social Links</h3>
        <div class="form-group">
          <label for="facebook_link">Site Facebook Link</label>
          <input type="text" class="form-control" name="facebook_link" id="facebook_link" aria-describedby="emailHelp" value="{{ $setting->facebook_link }}" placeholder="Enter site facebook link">
        </div>

        <div class="form-group">
          <label for="twitter_link">Site Twitter Link</label>
          <input type="text" class="form-control" name="twitter_link" id="twitter_link" aria-describedby="emailHelp" value="{{ $setting->twitter_link }}" placeholder="Enter site twitter link">
        </div>

        <div class="form-group">
          <label for="youtube_link">Site Youtube Link</label>
          <input type="text" class="form-control" name="youtube_link" id="youtube_link" aria-describedby="emailHelp" value="{{ $setting->youtube_link }}" placeholder="Enter site youtube link">
        </div>

        <div class="form-group">
          <label for="google_plus_link">Site Google Plus Link</label>
          <input type="text" class="form-control" name="google_plus_link" id="google_plus_link" aria-describedby="emailHelp" value="{{ $setting->google_plus_link }}" placeholder="Enter site google_plus link">
        </div>

        <div class="form-group">
          <label for="pinterest_link">Site Pinterest Link</label>
          <input type="text" class="form-control" name="pinterest_link" id="pinterest_link" aria-describedby="emailHelp" value="{{ $setting->pinterest_link }}" placeholder="Enter site pinterest link">
        </div>
        
        <div class="form-group">
          <label for="linked_in_link">Site LinkedIn Link</label>
          <input type="text" class="form-control" name="linked_in_link" id="linked_in_link" aria-describedby="emailHelp" value="{{ $setting->linked_in_link }}" placeholder="Enter site LinkedIn link">
        </div>
        
        <div class="form-group">
          <label for="instagram_link">Site Instagram Link</label>
          <input type="text" class="form-control" name="instagram_link" id="instagram_link" aria-describedby="emailHelp" value="{{ $setting->instagram_link }}" placeholder="Enter site instagram link">
        </div>

        <button type="submit" class="btn btn-primary"> <i class="fa fa-check"></i> Update Settings</button>
      </form>
    </div>
  </div>

@endsection
