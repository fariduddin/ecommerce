@extends('backend.layouts.master')

@section('content')
  <div class="card">
    <div class="card-header">
      <div class="float-left">Manage Pages</div>
      <div class="float-right"><a href="{{ route('admin.page.create') }}" class="btn btn-primary">Create a new page</a></div>
      <div class="clearfix"></div>
    </div>
    <div class="card-body">
      <table class="table table-hover table-striped" id="dataTable">
        <thead>
          <tr>
            <th>#</th>
            <th>Page Name</th>
            <th>Page Link</th>
            <th>Action</th>
          </tr>
        </thead>

        @foreach ($pages as $page)
          <tr>
            <td>{{ $loop->index+1 }}</td>
            <td>{{ $page->title }}</td>
            <td>
              <input type="text" name="" value="{{ url('/pages') }}/{{ $page->slug }}" class="form-control"/>
            </td>

            <td>
              <a href="{{ route('admin.page.edit', $page->id) }}" class="btn btn-success">Edit</a>

              <a href="#deleteModal{{ $page->id }}" data-toggle="modal" class="btn btn-danger">Delete</a>

              <!-- Delete Modal -->
              <div class="modal fade" id="deleteModal{{ $page->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Are sure to delete?</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form action="{!! route('admin.page.delete', $page->id) !!}"  method="post">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger">Permanent Delete</button>
                      </form>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>

            </td>
          </tr>
        @endforeach

      </table>
    </div>
  </div>
@endsection
