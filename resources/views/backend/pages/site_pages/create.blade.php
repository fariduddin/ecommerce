@extends('backend.layouts.master')

@section('stylesheets')
  <script src="{{ asset('js/tinymce/tiny_old.min.js') }}"></script>
  <script>
  // tinymce.init({ selector:'#description' });
  tinymce.init({
    selector: '#description',
    theme: 'modern',
    plugins: 'print preview code fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount   imagetools   contextmenu colorpicker textpattern help',
    toolbar1: 'formatselect | code bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
    image_advtab: true,
    templates: [
      { title: 'Test template 1', content: 'Test 1' },
      { title: 'Test template 2', content: 'Test 2' }
    ],
    content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css'
    ]
  });
  </script>
@endsection

@section('content')
  <div class="card">
    <div class="card-header">
      Add A New Site Page
    </div>
    <div class="card-body">
      <form action="{{ route('admin.page.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="title">Page Title</label>
          <input type="text" class="form-control" name="title" id="title" aria-describedby="emailHelp" placeholder="Enter Page Title">
        </div>

        <div class="form-group">
          <label for="description">Page Description</label>
          <textarea  class="form-control" name="description" id="description" aria-describedby="emailHelp" placeholder="Enter Page description"></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Add Page</button>
      </form>
    </div>
  </div>
@endsection
