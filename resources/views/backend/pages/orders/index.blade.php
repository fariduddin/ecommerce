@extends('backend.layouts.master')

@section('content')
  <div class="card">
    <div class="card-header">
      <div class="float-left">Manage Orders</div>
      <div class="float-right"><a href="{{ route('admin.orders.index') }}" class="btn btn-primary">See Unwatched Orderes</a></div>
      <div class="clearfix"></div>
    </div>
    <div class="card-body table-responsive">
      <table class="table table-hover table-striped" id="dataTable">
        <thead>
          <tr>
            <th>#</th>
            <th>Order Code</th>
            <th>Order Date</th>
            <th>Orderer Name</th>
            <th>Orderer Phone / Email</th>
            <th>Order Status</th>
            <th>Payment Information</th>
            <th>Action</th>
          </tr>
        </thead>

        @foreach ($orders as $order)
          <tr>
            <td>{{ $loop->index+1 }}</td>
            <td>PRF{{ $order->id }}</td>
            <td>
            {{ $order->created_at }}
            <hr>
            {{ $order->created_at->diffForHumans() }}
            </td>
            <td>{{ $order->name }}</td>
            <td>
              <a href="tel:{{ $order->phone }}">{{ $order->phone }}</a>
              <br>
              <a href="mailto:{{ $order->email }}">{{ $order->email }}</a>
            </td>
            <td>
              @if ($order->is_seen_by_admin)
                <span class="btn btn-success btn-sm">Seen</span>
              @else
                <span class="btn btn-danger btn-sm"><i class="fa fa-close"></i> Not Seen</span>
              @endif
              <hr>

              @if ($order->is_paid)
                <span class="btn btn-success btn-sm"><i class="fa fa-check"></i> Paid</span>
              @else
                <span class="btn btn-danger btn-sm"><i class="fa fa-close"></i> Not Paid</span>
              @endif
              <hr>

              @if ($order->is_completed)
                <span class="btn btn-success btn-sm"><i class="fa fa-check"></i> completed</span>
              @else
                <span class="btn btn-danger btn-sm"><i class="fa fa-close"></i> Not completed</span>
              @endif

            </td>
            <td>
              <strong>Payment: </strong> {{ $order->payment->payment->name }} <br>
              <strong>Account Type: </strong> {{ $order->payment->payment->type }} <br>
              <strong>Transaction ID: </strong> {{ $order->payment->transaction_id }} <br>
            </td>
            <td>
              <a href="{{ route('admin.orders.show', $order->id) }}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i> View</a>
              <br>

              <form action="{{ route('admin.orders.make_paid', $order->id) }}" style="margin-top: 10px" method="post">
                @csrf
                @if ($order->is_paid)
                  <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-close"></i> Cancel Payment</button>
                @else
                  <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> confirm payment</button>
                @endif
              </form>
              <form action="{{ route('admin.orders.make_completed', $order->id) }}" style="margin-top: 10px" method="post">
                @csrf
                @if ($order->is_completed)
                  <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-close"></i> Cancel Complete</button>
                @else
                  <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Complete Order</button>
                @endif
              </form>
              {{-- <a href="{{ route('admin.product.edit', $product->id) }}" class="btn btn-success">Edit</a>
              <a href="#deleteModal{{ $product->id }}" data-toggle="modal" class="btn btn-danger">Delete</a>

              <!-- Delete Modal -->
              <div class="modal fade" id="deleteModal{{ $product->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Are sure to delete?</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form action="{!! route('admin.product.delete', $product->id) !!}"  method="post">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger">Permanent Delete</button>
                      </form>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                  </div>
                </div>
              </div> --}}

            </td>
          </tr>
        @endforeach

      </table>
    </div>
  </div>
@endsection
