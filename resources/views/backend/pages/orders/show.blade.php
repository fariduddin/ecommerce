@extends('backend.layouts.master')

@section('content')
  <div class="card">
    <div class="card-header">
      <div class="float-left">Manage Orders</div>
      <div class="float-right"><a href="{{ route('admin.product.create') }}" class="btn btn-primary">See Unwatched Orderes</a></div>
      <div class="clearfix"></div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-7 border-bottom">
          <h3>Order Informations</h3>
          <p>
            <strong>Order CODE: </strong>
            <br>
            PRF{{ $order->id }}
          </p>
          <p>
          <strong>Order Date: </strong>
          {{ $order->created_at }}
            <br>
            {{ $order->created_at->diffForHumans() }}
          </p>
          
          <h3>Orderer Informations</h3>
          <p>
            <strong>Orderer Name: </strong>
            <br>
            {{ $order->name }}
          </p>
          <p>
            <strong>Orderer Phone No: </strong>
            <br>
            {{ $order->phone }}
          </p>
          <p>
            <strong>Orderer Email: </strong>
            <br>
            {{ $order->email }}
          </p>
          <p>
            <strong>Orderer Shipping Address: </strong>
            <br>
            {{ $order->shipping_address }}
          </p>
          <p>
            @if ($order->user_id != NULL)
              {{-- <a href="{{ route('admin.users.show', $order->user->username) }}" class="btn btn-primary btn-sm"><i class="fa fa-user"></i> View User</a> --}}
            @endif
          </p>
        </div>
        <div class="col-md-5 border-bottom">
          <h3>Order Informations</h3>
          <strong>Payment: </strong> {{ $order->payment->payment->name }} <br>
          <strong>Account Type: </strong> {{ $order->payment->payment->type }} <br>
          <strong>Transaction ID: </strong> {{ $order->payment->transaction_id }} <br>
          
        </div>
        <div class="col-md-12">
          <h3>Order Item Informations</h3>
          <table class="table">
            <thead>
              <tr>
                <th width="5%">No.</th>
                <th width="10%">Item</th>
                <th width="20%">Name/Link</th>
                <th width="40%">
                  Quantity/Size/Color
                </th>
                <th width="10%">Unit Price</th>
                <th width="10%">Total Price</th>
                <th width="15%">Action</th>
              </tr>
            </thead>

            <tbody>
              @php
              $final_price = 0;
              @endphp
              @foreach ($order->carts as $item)

                <tr>

                  <td>
                    {{ $loop->index+1 }}
                  </td>
                  <td>
                    <img src="{{ asset('/images/products/'. $item->product->images->first->image->image) }}" class="img img-responsive" alt="{{ $item->product->title }}" width="50">

                  </td>
                  <td>
                    <strong>
                      {{ $item->product->title }}
                    </strong>
                  </td>

                  <td>
                    <form action="{{ route('carts.update', $item->id) }}" method="post">
                      @csrf
                      <div class="row">
                        <div class="col-sm-3">
                          <input type="number" min="1" name="product_quantity" value="{{ $item->product_quantity }}" class="form-control"/>
                        </div>
                        <div class="col-sm-4">
                          @if ($item->product->sizes->count() > 0)
                            <select class="form-control" name="product_size_id">
                              <option value="">Please select a size</option>
                              @foreach ($item->product->sizes as $size)
                                <option value="{{ $size->id }}" {{ $size->id == $item->product_size_id ? 'selected' : '' }}>{{ $size->size }}</option>
                              @endforeach
                            </select>
                          @endif
                        </div>
                        <div class="col-sm-4">
                          @if ($item->product->colors->count() > 0)
                            <select class="form-control" name="product_color_id">
                              <option value="">Please select a color</option>
                              @foreach ($item->product->colors as $color)
                                <option value="{{ $color->color->id }}"  {{ $color->color->id == $item->product_color_id ? 'selected' : '' }}>{{ $color->color->name }}</option>
                              @endforeach
                            </select>
                          @endif
                        </div>
                      </div>


                      <button type="submit" class="btn btn-success btn-sm" style="margin-top: 5px; margin-left: 20%"><i class="fa fa-check"></i> Update</button>
                    </form>
                  </td>
                  <td>
                    @php
                    if ($item->product::has_discount($item->product->id)){
                      $product_real_price = $item->product::current_price($item->product->id);
                    }else {
                      $product_real_price = $item->product->price;
                    }
                    @endphp
                    @if ($item->product::has_discount($item->product->id))
                      <del style="color: red">{{ $item->product->price }}</del> ({{ $item->product->offer_discount }}% off)
                    @endif
                    <br>
                    {{ $product_real_price }} Taka
                  </td>
                  <td>
                    <p class="total_ammount_p1">
                      @php
                      $product_total_price =  $product_real_price * $item->product_quantity;
                      $final_price += $product_total_price;
                      @endphp
                      {{ $product_total_price }} Taka
                    </p>
                  </td>
                  <td>
                    <span class="cart-product-option">

                      <form action="{{ route('carts.delete', $item->id) }}" method="post" accept-charset="utf-8">
                        @csrf
                        <button type="submit" class="btn btn-danger btn-sm"> <i class="fa fa-trash"></i> Remove Item
                        </form>

                      </span>
                    </td>

                  </tr>
                @endforeach

                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td colspan="1"><strong>Total:</strong></td>
                  <td></td>
                  <td></td>
                  <td>
                    <p><span class="total_product_sum">{{ $final_price }} Taka</span></p>
                  </td>
                  <div class="clearfix"></div>
                </tr>


                </tbody>
              </table>
              <div class="pull-right mr-5">
              <h4>
                Inside Dhaka Total Cost- {{ $final_price + App\Models\Setting::first()->shipping_cost_inside_dhaka }} Taka
                <br>
                Outside Dhaka Total Cost- {{ $final_price + App\Models\Setting::first()->shipping_cost_outside_dhaka }} Taka
              </h4>
              </div>
              <div class="clearfix"></div>
              <hr>
              <div class="float-right">
                <form action="{{ route('admin.orders.make_paid', $order->id) }}" style="display:inline; margin-top: 10px" method="post">
                  @csrf
                  @if ($order->is_paid)
                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-close"></i> Cancel Payment</button>
                  @else
                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> confirm payment</button>
                  @endif
                </form>
                <form action="{{ route('admin.orders.make_completed', $order->id) }}" style="display:inline; margin-top: 10px" method="post">
                  @csrf
                  @if ($order->is_completed)
                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-close"></i> Cancel Complete</button>
                  @else
                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Complete Order</button>
                  @endif
                </form>
              </div>
              <div class="clearfix"></div>

            </div>
          </div>
        </div>
      </div>
    @endsection
