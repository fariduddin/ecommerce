@extends('backend.layouts.master')

@section('content')
      <div class="card">
        <div class="card-header">
          Add Category
        </div>
        <div class="card-body">
          <form action="{{ route('admin.category.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" name="name" id="name" aria-describedby="emailHelp" placeholder="Enter Category Name">
            </div>

            <div class="form-group">
              <label for="exampleInputPassword1">Description</label>
              <textarea name="description" rows="8" cols="80" class="form-control"></textarea>

            </div>

            <div class="form-group">
              <label for="exampleInputPassword1">Parent Category (optional)</label>
              <select class="form-control" name="parent_id">
                <option value="">Please select a Parent category</option>
                @foreach ($main_categories as $category)
                  <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label for="exampleInputPassword1">Is Category Gender wise (only applicable for Parent Category)</label>
              <select class="form-control" name="is_gender_wise">
                <option value="1">Yes</option>
                <option value="0" selected>No</option>
              </select>
            </div>

            <div class="form-group">
              <label for="exampleInputPassword1">Gender type (only applicable for sub Category which is gender wise)</label>
              <select class="form-control" name="gender">
                <option value="">Null</option>
                <option value="1">Boys</option>
                <option value="0">Girls</option>
              </select>
            </div>

            <div class="form-group">
              <label for="image">Category Image (optional)</label>
              <input type="file" class="form-control" name="image" id="image" >
            </div>


            <button type="submit" class="btn btn-primary">Add Category</button>
          </form>
        </div>
      </div>
@endsection
