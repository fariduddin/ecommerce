@extends('backend.layouts.master')

@section('stylesheets')

@endsection

@section('content')
  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="{!! route('admin.dashboard') !!}">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Edit My Profile</li>
  </ol>

  <div class="clearfix"></div>
  <div class="admin-page card card-body">
    <h2 class="text-info mb-4">Edit My Profile</h2>
    <form class="form-group" action="{{ route('admin.myProfileUpdate') }}" method="POST" enctype="multipart/form-data">
      @csrf

      <div class="form-row">
        <div class="col-md-6">
          <div class="form-group mt-4">
            <label for="name">Admin Name <span class="text-danger"><b>(required)</b></span></label>
            <input type="text" class="form-control" placeholder="Admin Name" name="name" id="name" value="{{ $admin->name }}">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group mt-4">
            <label for="email">Admin Email <span class="text-danger"><b>(required)</b></span></label>
            <input type="text" class="form-control" placeholder="Admin Email" name="email" id="email" value="{{ $admin->email }}">
          </div>
        </div>
      </div>

      <div class="form-row">
        <div class="col-md-6">
          <div class="form-group mt-4">
            <label for="mobile_no">Admin Mobile No <span class="text-danger"><b>(required)</b></span></label>
            <input type="text" class="form-control" placeholder="Admin Mobile No" name="mobile_no" id="mobile_no" value="{{ $admin->mobile_no }}">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group mt-4">
            <label for="phone_no">Admin Phone No <span class="text-info"><b>(optional)</b></span></label>
            <input type="text" class="form-control" placeholder="Admin Phone No" name="phone_no" id="phone_no" value="{{ $admin->phone_no }}">
          </div>
        </div>
      </div>
      <div class="form-row">
        <div class="col-md-6">
          <div class="form-group mt-4">
            <label for="password">Admin Password <span class="text-info"><b>(optional)</b></span></label>
            <input type="text" class="form-control" placeholder="Admin Password" name="password" id="password">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group mt-4">
            <label for="password_confirmation">Confirm Password <span class="text-info"><b>(optional)</b></span></label>
            <input type="text" class="form-control" placeholder="Confirm Password" name="password_confirmation" id="password_confirmation">
          </div>
        </div>
      </div>

      <div class="form-row">
        <div class="col-md-6">
          <div class="form-group mt-4">
            <label for="image">Admin Image <span class="text-info">(Optional)</span> </label>
            @if ($admin->image != NULL)
              <p>
                <strong>Previous Image: </strong>
                <img src="{{ asset('images/admins/'.$admin->image) }}" width="100">
              </p>
            @endif
            <input type="file" name="image" id="image" class="form-control" />
          </div>
        </div>
      </div>

      <button type="submit" class="btn btn-primary float-left mt-3 btn-lg" name="submit"><i class="fa fa-check"></i> Update My Profile</button>
      <div class="clearfix"></div>
    </form>
  </div>

@endsection


@section('scripts')
  <script>
  vm = this;
  tinymce.init({
    selector:'#about' ,
    // plugins:'link code image imagetools',
    plugins: 'table code print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern',
    // plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link  codesample  hr textcolor wordcount help',
    toolbar: 'table code autolink bold italic hr forecolor backcolor | link  image | alignleft aligncenter alignright | numlist bullist | preview',

    image_advtab: true,
    menubar:false,

    setup: function(editor){
      editor.on('keyup', function(e){
        vm.model = editor.getContent();
      })
    }
  });
  </script>

@endsection
