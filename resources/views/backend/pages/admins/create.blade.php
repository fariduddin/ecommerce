@extends('backend.layouts.master')

@section('stylesheets')

@endsection

@section('content')
  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="{!! route('admin.dashboard') !!}">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">
      <a href="{{ route('admin.getAdmins') }}">Manage Admins</a>
    </li>
    <li class="breadcrumb-item active">Add New Admin</li>
  </ol>

  <div class="clearfix"></div>
  <div class="admin-page card card-body">
    <h2 class="text-info mb-4">Add a new admin</h2>
    <form class="form-group" action="{{ route('admin.store') }}" method="POST" enctype="multipart/form-data">
      @csrf

      <div class="form-row">
        <div class="col-md-6">
          <div class="form-group mt-4">
            <label for="name">Admin Name <span class="text-danger"><b>(required)</b></span></label>
            <input type="text" class="form-control" placeholder="Admin Name" name="name" id="name">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group mt-4">
            <label for="email">Admin Email <span class="text-danger"><b>(required)</b></span></label>
            <input type="text" class="form-control" placeholder="Admin Email" name="email" id="email">
          </div>
        </div>
      </div>

      <div class="form-row">
        <div class="col-md-6">
          <div class="form-group mt-4">
            <label for="mobile_no">Admin Mobile No <span class="text-danger"><b>(required)</b></span></label>
            <input type="text" class="form-control" placeholder="Admin Mobile No" name="mobile_no" id="mobile_no">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group mt-4">
            <label for="phone_no">Admin Phone No <span class="text-info"><b>(optional)</b></span></label>
            <input type="text" class="form-control" placeholder="Admin Phone No" name="phone_no" id="phone_no">
          </div>
        </div>
      </div>
      <div class="form-row">
        <div class="col-md-6">
          <div class="form-group mt-4">
            <label for="password">Admin Password <span class="text-danger"><b>(required)</b></span></label>
            <input type="text" class="form-control" placeholder="Admin Password" name="password" id="password">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group mt-4">
            <label for="password_confirmation">Confirm Password <span class="text-danger"><b>(required)</b></span></label>
            <input type="text" class="form-control" placeholder="Confirm Password" name="password_confirmation" id="password_confirmation">
          </div>
        </div>
      </div>

      <div class="form-row">
        <div class="col-md-6">
          <div class="form-group mt-4">
            <label for="image">Admin Image <span class="text-info">(Optional)</span> </label>
            <input type="file" name="image" id="image" class="form-control" />
          </div>
        </div>
      </div>

      <button type="submit" class="btn btn-primary float-left mt-3 btn-lg" name="submit">Add Admin</button>
      <div class="clearfix"></div>
    </form>
  </div>

@endsection


@section('scripts')
  <script>
  vm = this;
  tinymce.init({
    selector:'#about' ,
    // plugins:'link code image imagetools',
    plugins: 'table code print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern',
    // plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link  codesample  hr textcolor wordcount help',
    toolbar: 'table code autolink bold italic hr forecolor backcolor | link  image | alignleft aligncenter alignright | numlist bullist | preview',

    image_advtab: true,
    menubar:false,

    setup: function(editor){
      editor.on('keyup', function(e){
        vm.model = editor.getContent();
      })
    }
  });
  </script>

@endsection
