@extends('backend.layouts.master')

@section('content')

  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="{!! route('admin.dashboard') !!}">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Manage Admins</li>
  </ol>

  {{-- Data Table --}}
  <div class="card mb-3">
    <div class="card-header">
      <i class="fa fa-table"></i> Admins Of {{ config('app.name') }}
      <a href="{{ route('admin.create') }}" class="btn btn-primary float-right">
        <i class="fa fa-fw fa-plus"></i> New Admin
      </a></div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Name</th>
                <th>Phone No</th>
                <th>Mobile No</th>
                <th>Email</th>
                <th>Image</th>
                <th>Manage</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>Name</th>
                <th>Phone No</th>
                <th>Mobile No</th>
                <th>Email</th>
                <th>Image</th>
                <th>Manage</th>
              </tr>
            </tfoot>
            <tbody>
              @foreach($admins as $admin)
                <tr>
                  <td> {{ $admin->name }} </td>
                  <td> {{ $admin->phone_no }} </td>
                  <td> {{ $admin->mobile_no }} </td>
                  <td> {{ $admin->email }} </td>
                  <td>
                    @if ($admin->image != NULL)
                      <img src="{{ asset('images/admins/'.$admin->image) }}" width="100">
                    @endif
                  </td>

                  <td>
                    <a class="btn btn-outline-primary" href="{{ route('admin.edit', $admin->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                    <button type="submit" data-toggle="modal" data-target="#deleteModal{{ $admin->id }}" class="btn btn-outline-danger"><i class="fa fa-fw fa-trash"></i> Delete</button>
                    {{-- Delete Notice Modal --}}
                    <div class="modal fade" id="deleteModal{{ $admin->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="deleteModalLabel">Are you want to delete this data?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-footer">
                            <form class="" action="{{ route('admin.delete', $admin->id) }}" method="post">
                              @csrf
                              <button type="submit" class="btn btn-outline-danger"><i class="fa fa-trash"></i> Delete</button>
                            </form>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
              @endforeach

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

@endsection
