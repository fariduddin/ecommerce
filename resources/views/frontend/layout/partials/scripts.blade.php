<!-- Plugins JS File -->
<script src="{{asset('public/assets/js/jquery.min.js')}}"></script>
<script src="{{asset('public/assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('public/assets/js/plugins.min.js')}}"></script>

<!-- Main JS File -->
<script src="{{asset('public/assets/js/main.min.js')}}"></script>

@yield('scripts')
