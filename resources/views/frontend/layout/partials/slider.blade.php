<div class="home-top-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="home-slider owl-carousel owl-carousel-lazy">
                    <div class="home-slide">
                        <img class="owl-lazy" src="public/assets/images/lazy.png"
                            data-src="public/assets/images/slider/slide-1.jpg" alt="slider image">
                        <div class="home-slide-content">
                            <h1>up to 40% off</h1>
                            <h3>woman clothing</h3>
                            <a href="category.html" class="btn btn-primary">Shop Now</a>
                        </div><!-- End .home-slide-content -->
                    </div><!-- End .home-slide -->

                    <div class="home-slide">
                        <img class="owl-lazy" src="public/assets/images/lazy.png"
                            data-src="public/assets/images/slider/slide-2.jpg" alt="slider image">
                        <div class="home-slide-content">
                            <h1>up to 50% off</h1>
                            <h3>New collection</h3>
                            <a href="category.html" class="btn btn-secondary">Shop Now</a>
                        </div><!-- End .home-slide-content -->
                    </div><!-- End .home-slide -->
                </div><!-- End .home-slider -->
            </div><!-- End .col-lg-8 -->

            <div class="col-lg-4 top-banners">
                <div class="banner banner-image">
                    <a href="#">
                        <img src="public/assets/images/banners/banner-1.jpg" alt="banner">
                    </a>
                </div><!-- End .banner -->

                <div class="banner banner-image">
                    <a href="#">
                        <img src="public/assets/images/banners/banner-2.jpg" alt="banner">
                    </a>
                </div><!-- End .banner -->

                <div class="banner banner-image">
                    <a href="#">
                        <img src="public/assets/images/banners/banner-3.jpg" alt="banner">
                    </a>
                </div><!-- End .banner -->
            </div><!-- End .col-lg-4 -->
        </div><!-- End .row -->
    </div><!-- End .container -->
</div><!-- End .home-top-container -->