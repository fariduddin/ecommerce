<!-- Favicon -->
<link rel="icon" type="image/x-icon" href="{{ asset('public/assets/images/icons/favicon.ico') }}">

<!-- Plugins CSS File -->
<link rel="stylesheet" href="{{ asset('public/assets/css/bootstrap.min.css') }}">

<!-- Main CSS File -->
<link rel="stylesheet" href="{{ asset('public/assets/css/style.min.css') }}">
