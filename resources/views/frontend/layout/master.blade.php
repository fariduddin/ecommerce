<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from portotheme.com/html/porto/demo-8/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 16 Nov 2018 10:34:18 GMT -->

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Porto - Bootstrap eCommerce Template</title>

    @include('frontend.layout.partials.meta_tags')
    @include('frontend.layout.partials.styles')

</head>

<body>
    <div class="page-wrapper">

        @include('frontend.layout.partials.header')

        @yield('main-content')
    </div><!-- End .page-wrapper -->

    @include('frontend.layout.partials.footer')


    <a id="scroll-top" href="#top" title="Top" role="button"><i class="icon-angle-up"></i></a>

    @include('frontend.layout.partials.mobile_menu')

    @include('frontend.layout.partials.news_letter')

    @include('frontend.layout.partials.scripts')
</body>

<!-- Mirrored from portotheme.com/html/porto/demo-8/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 16 Nov 2018 10:34:53 GMT -->

</html>