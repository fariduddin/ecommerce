<link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/data-table/data-table.css') }}">
<link rel="stylesheet" href="{{ asset('css/animate/animate.css') }}">
<link rel="stylesheet" href="{{ asset('css/admin/theme.css') }}"> <!-- Botstrap Theme Style -->
<link rel="stylesheet" href="{{ asset('css/admin/style.css') }}"> <!-- Custom Style -->

<link rel="shortcut icon" href="{{ asset('images/settings/'.\App\Models\Setting::first()->favicon) }}">


<!-- Font Awesome Icons -->
{{-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> --}}
<link href="{!! asset('css/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet">


<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300' rel='stylesheet' type='text/css'>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
