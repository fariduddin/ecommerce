<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
  <a class="navbar-brand" href="{!! route('admin.dashboard') !!}">Tashfia Fashion House</a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarResponsive">
    <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
      <li class="nav-item {{ Route::is('admin.dashboard') ? 'sidenav-active' : '' }}" data-toggle="tooltip" data-placement="right" title="Dashboard">
        <a class="nav-link" href="{!! route('admin.dashboard') !!}">
          <i class="fa fa-fw fa-dashboard"></i>
          <span class="nav-link-text">Dashboard</span>
        </a>
      </li>

      <li class="nav-item {{ (Route::is('admin.orders.index')|| Route::is('admin.orders.seen') || Route::is('admin.orders.completed') || Route::is('admin.orders.all') || Route::is('admin.orders.show')) ? 'sidenav-active' : '' }}" data-toggle="tooltip" data-placement="right" title="Manage Orders">
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#manageOrders" data-parent="#exampleAccordion">
          <i class="fa fa-fw fa-user"></i>
          <span class="nav-link-text">Manage Orders
            <span class="badge badge-danger">{{ App\Models\Order::where('is_seen_by_admin', 0)->get()->count() }}</span>
          </span>
        </a>
        <ul class="sidenav-second-level collapse {{ (Route::is('admin.orders.index')|| Route::is('admin.orders.show') || Route::is('admin.orders.all') || Route::is('admin.orders.completed') || Route::is('admin.orders.seen')) ? 'show' : '' }}" id="manageOrders">
          <li>
            <a href="{!! route('admin.orders.index') !!}">Manage Unseen Orders
              <span class="badge badge-danger">{{ App\Models\Order::where('is_seen_by_admin', 0)->get()->count() }}</span>
            </a>
          </li>
          <li>
            <a href="{!! route('admin.orders.all') !!}">Manage All Orders</a>
          </li>
          <li>
            <a href="{!! route('admin.orders.seen') !!}">Manage seen Orders</a>
          </li>
          <li>
            <a href="{!! route('admin.orders.completed') !!}">Manage completed Orders</a>
          </li>
        </ul>
      </li>


      <li class="nav-item {{ (Route::is('admin.products')|| Route::is('admin.product.create') || Route::is('admin.product.edit')) ? 'sidenav-active' : '' }}" data-toggle="tooltip" data-placement="right" title="Manage Products">
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#manageProducts" data-parent="#exampleAccordion">
          <i class="fa fa-fw fa-user"></i>
          <span class="nav-link-text">Manage Products</span>
        </a>
        <ul class="sidenav-second-level collapse {{ (Route::is('admin.products')|| Route::is('admin.product.create') || Route::is('admin.product.edit')) ? 'show' : '' }}" id="manageProducts">
          <li>
            <a href="{!! route('admin.products') !!}">Manage Products</a>
          </li>
          <li>
            <a href="{!! route('admin.product.create') !!}">Add Product</a>
          </li>
        </ul>
      </li>


  <li class="nav-item {{ (Route::is('admin.getAdmins') || Route::is('admin.create')) ? 'sidenav-active' : '' }}" data-toggle="tooltip" data-placement="right" title="Settings">
    <a class="nav-link" href="{{ route('admin.getAdmins') }}">
      <i class="fa fa-fw fa-book"></i>
      <span class="nav-link-text">Manage Admins</span>
    </a>
  </li>


      {{--
      <li class="nav-item {{ (Route::is('admin.navigation_menus') || Route::is('admin.navigation_menu.create') || Route::is('admin.navigation_menu.edit')) ? 'sidenav-active' : '' }}" data-toggle="tooltip" data-placement="right" title="Settings">
      <a class="nav-link" href="{{ route('admin.navigation_menus') }}">
      <i class="fa fa-fw fa-tags"></i>
      <span class="nav-link-text">Manage Navigation Menu</span>
    </a>
  </li> --}}

  <li class="nav-item {{ (Route::is('admin.categories') || Route::is('admin.category.create') || Route::is('admin.category.edit')) ? 'sidenav-active' : '' }}" data-toggle="tooltip" data-placement="right" title="Settings">
    <a class="nav-link" href="{{ route('admin.categories') }}">
      <i class="fa fa-fw fa-tags"></i>
      <span class="nav-link-text">Manage Category</span>
    </a>
  </li>

  <li class="nav-item {{ (Route::is('admin.brands') || Route::is('admin.brand.create') || Route::is('admin.brand.edit')) ? 'sidenav-active' : '' }}" data-toggle="tooltip" data-placement="right" title="Settings">
    <a class="nav-link" href="{{ route('admin.brands') }}">
      <i class="fa fa-fw fa-book"></i>
      <span class="nav-link-text">Manage Brand</span>
    </a>
  </li>

  <li class="nav-item {{ (Route::is('admin.payments') || Route::is('admin.payment.create') || Route::is('admin.payment.edit')) ? 'sidenav-active' : '' }}" data-toggle="tooltip" data-placement="right" title="Settings">
    <a class="nav-link" href="{{ route('admin.payments') }}">
      <i class="fa fa-fw fa-book"></i>
      <span class="nav-link-text">Manage Payment</span>
    </a>
  </li>


  <li class="nav-item {{ (Route::is('admin.colors') || Route::is('admin.color.create') || Route::is('admin.color.edit')) ? 'sidenav-active' : '' }}" data-toggle="tooltip" data-placement="right" title="Settings">
    <a class="nav-link" href="{{ route('admin.colors') }}">
      <i class="fa fa-fw fa-book"></i>
      <span class="nav-link-text">Manage Color</span>
    </a>
  </li>

  <li class="nav-item {{ (Route::is('admin.divisions') || Route::is('admin.division.create') || Route::is('admin.division.edit')) ? 'sidenav-active' : '' }}" data-toggle="tooltip" data-placement="right" title="Settings">
    <a class="nav-link" href="{{ route('admin.divisions') }}">
      <i class="fa fa-fw fa-book"></i>
      <span class="nav-link-text">Manage Division</span>
    </a>
  </li>

  <li class="nav-item {{ (Route::is('admin.districts') || Route::is('admin.district.create') || Route::is('admin.district.edit')) ? 'sidenav-active' : '' }}" data-toggle="tooltip" data-placement="right" title="Settings">
    <a class="nav-link" href="{{ route('admin.districts') }}">
      <i class="fa fa-fw fa-book"></i>
      <span class="nav-link-text">Manage District</span>
    </a>
  </li>

  <li class="nav-item {{ (Route::is('admin.sliders.index')) ? 'sidenav-active' : '' }}" data-toggle="tooltip" data-placement="right" title="Settings">
    <a class="nav-link" href="{{ route('admin.sliders.index') }}">
      <i class="fa fa-fw fa-cog"></i>
      <span class="nav-link-text">Manage Sliders</span>
    </a>
  </li>

  <li class="nav-item {{ (Route::is('admin.pages')|| Route::is('admin.page.create') || Route::is('admin.page.edit')) ? 'sidenav-active' : '' }}" data-toggle="tooltip" data-placement="right" title="Manage Products">
    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#managePages" data-parent="#exampleAccordion">
      <i class="fa fa-fw fa-user"></i>
      <span class="nav-link-text">Manage Pages</span>
    </a>
    <ul class="sidenav-second-level collapse {{ (Route::is('admin.pages')|| Route::is('admin.page.create') || Route::is('admin.page.edit')) ? 'show' : '' }}" id="managePages">
      <li>
        <a href="{!! route('admin.pages') !!}">Manage Pages</a>
      </li>
      <li>
        <a href="{!! route('admin.page.create') !!}">Add Page</a>
      </li>
    </ul>
  </li>

<li class="nav-item {{ (Route::is('admin.reviews.index')) ? 'sidenav-active' : '' }}" data-toggle="tooltip" data-placement="right" title="Careers">
    <a class="nav-link" href="{{ route('admin.reviews.index') }}">
      <i class="fa fa-fw fa-cog"></i>
      <span class="nav-link-text">Manage Product Reviews
    </a>
  </li>
  
  <li class="nav-item {{ (Route::is('admin.settings.index') || Route::is('admin.settings.create')) ? 'sidenav-active' : '' }}" data-toggle="tooltip" data-placement="right" title="Settings">
    <a class="nav-link" href="{{ route('admin.settings.index') }}">
      <i class="fa fa-fw fa-cog"></i>
      <span class="nav-link-text">Settings</span>
    </a>
  </li>

  <li class="nav-item {{ (Route::is('admin.careers.index')) ? 'sidenav-active' : '' }}" data-toggle="tooltip" data-placement="right" title="Careers">
    <a class="nav-link" href="{{ route('admin.careers.index') }}">
      <i class="fa fa-fw fa-cog"></i>
      <span class="nav-link-text">Manage Upcoming Careers <span class="badge badge-danger">{{ App\Models\Career::where('status', 0)->get()->count() }}</span></span>
    </a>
  </li>
  <li class="nav-item {{ Route::is('admin.changePasswordForm') ? 'sidenav-active' : '' }}" data-toggle="tooltip" data-placement="right" title="Change Password">
    <a class="nav-link" href="{{ route('admin.changePasswordForm') }}">
      <i class="fa fa-fw fa-cog"></i>
      <span class="nav-link-text">Change Password</span>
    </a>
  </li>

</ul>
<ul class="navbar-nav sidenav-toggler">
  <li class="nav-item">
    <a class="nav-link text-center" id="sidenavToggler">
      <i class="fa fa-fw fa-angle-left"></i>
    </a>
  </li>
</ul>
<ul class="navbar-nav">
  <li class="nav-item">
    <a class="nav-link" target="_blank" href="{!! route('index') !!}">
      <i class="fa fa-fw fa-eye"></i> View Site</a>
    </li>
  </ul>
  <ul class="navbar-nav ml-auto">
    {{-- <li class="nav-item dropdown mr-5">
    @php
    $allContact = \App\Models\Contact::where('seen', 0)->get();
    $count = count($allContact);
  @endphp
  <a class="nav-link dropdown-toggle mr-5" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  <i class="fa fa-fw fa-envelope float-left incoming-message-sign"></i>
  <span class="badge badge-primary float-right incoming-message">{{ $count }}</span>
</a>
<div class="clearfix"></div>
<div class="dropdown-menu" aria-labelledby="messagesDropdown">
<h6 class="dropdown-header">New Messages:</h6>
<div class="dropdown-divider"></div>
<a class="dropdown-item" href="#">
@foreach(\App\Models\Contact::where('seen', 0)->get() as $message)
<a href="{{ route('admin.contact.show', $message->id) }}" class="pl-3">
<strong>Message From - {{ $message->name }} <br /> </strong>
</a>
<span class="text-muted ml-4">({{ $message->email }})</span>
<div class="dropdown-divider"></div>
@endforeach
</a>

<a class="dropdown-item small" href="{{ route('admin.contact.index') }}">View all messages</a>
</div>
</li> --}}
<li class="nav-item dropdown">
  <div class="dropdown-menu" aria-labelledby="alertsDropdown">

    <li class="nav-item">
      <a class="nav-link" data-toggle="modal" data-target="#adminLogoutModal"><i class="fa fa-sign-out"></i>Logout</a>
      <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
        @csrf
      </form>
    </li>

   <li class="nav-item">
      <a class="nav-link" href="{{ route('admin.myProfile') }}"><i class="fa fa-user"></i>My Profile</a>
   </li>


  </ul>
</div>
</nav>


{{-- Logout Modal --}}
<div class="modal fade" id="adminLogoutModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Are you want to logout ?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <form class="" action="{!! route('admin.logout') !!}" method="post">
          @csrf
          <button type="submit" class="btn btn-outline-danger" onclick="event.preventDefault();
          document.getElementById('logout-form').submit();"><i class="fa fa-trash"></i> Logout</button>
        </form>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
