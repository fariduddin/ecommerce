<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
    public function Payment()
    {
      return $this->belongsTo(Payment::class);
    }
}
