<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Product;
use App\Models\User;
use Auth;

class Cart extends Model
{
  public function user()
  {
    return $this->belongsTo(User::class);
  }
  public function product()
  {
    return $this->belongsTo(Product::class);
  }
  public function size()
  {
    return $this->belongsTo(ProductSize::class);
  }
  public function color()
  {
    return $this->belongsTo(ProductColor::class);
  }

  public static function total_items()
  {
    if (Auth::check()) {
      $user_id = Auth::id();
      $ip_address = User::find($user_id)->ip_address;
      $items = Cart::where('user_id', $user_id)
      ->where('order_id', NULL)
      ->get();
    }else {
      $items = Cart::orWhere('ip_address', request()->ip())
      ->where('order_id', NULL)
      ->get();
    }

    return $items;
  }

  public static function count_items()
  {
    $total = 0;
    if (Auth::check()) {
      $user_id = Auth::id();
      $ip_address = User::find($user_id)->ip_address;
      $items = Cart::where('user_id', $user_id)
      ->where('order_id', NULL)
      ->get();
      foreach ($items as $item) {
        $total+=$item->product_quantity;
      }
    }else {
      $items = Cart::orWhere('ip_address', request()->ip())
      ->where('order_id', NULL)
      ->get();
      foreach ($items as $item) {
        $total+=$item->product_quantity;
      }
    }

    return $total;
  }

}
