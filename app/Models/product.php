<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
  protected $fillable = [
    'title',
    'price',
    'offer_discount',
    'offer_last_date',
    'description'
  ];
  public function images()
  {
    return $this->hasMany(ProductImage::class);
  }

  public function category()
  {
    return $this->belongsTo(Category::class);
  }

  public function brand()
  {
    return $this->belongsTo(Brand::class);
  }

  public function sizes()
  {
    return $this->hasMany(ProductSize::class);
  }

  public function colors()
  {
    return $this->hasMany(ProductColor::class);
  }
  
  
  public function reviews()
  {
    return $this->hasMany(Review::class);
  }


  public static function current_price($product_id)
  {
    $product = Product::find($product_id);
    if ($product->offer_discount != NULL){
      if (date('Y-m-d') < $product->offer_last_date){
        $discount_amount = $product->price * $product->offer_discount / 100;
        $price = $product->price - $discount_amount;
      }else {
        $price = $product->price;
      }
    }else {
      $price = $product->price;
    }
    return $price;
  }

  public static function has_discount($product_id)
  {
    $product = Product::find($product_id);
    if ($product->offer_discount != NULL){
      if (date('Y-m-d') < $product->offer_last_date){
        return true;
      }else {
        return false;
      }
    }else {
      return false;
    }
  }

  public function hasColor($color_id)
  {
    $product_id = $this->id;
    $product_colors = DB::table('product_colors')->where('product_id', $product_id)->where('color_id', $color_id)->first();
    if (!is_null($product_colors)) {
      return true;
    }
    return false;
  }

  public function hasNotSize($size)
  {
    $product_id = $this->id;
    $product_size = DB::table('product_sizes')->where('product_id', $product_id)->where('size', $size)->first();
    if (!is_null($product_size)) {
      return false;
    }
    return true;
  }
}
