<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\NavigationMenu;
use Image;
use File;

class NavigationMenusController extends Controller
{

  // public function __construct(){
  //   $this->middleware('auth:admin');
  // }

  public function index()
  {
    $navigation_menus = NavigationMenu::orderBy('id', 'desc')->get();
    return view('backend.pages.navigation_menus.index', compact('navigation_menus'));
  }

  public function create()
  {
    return view('backend.pages.navigation_menus.create');
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'title'  => 'required',
      'link'  => 'nullable|url',
    ],
    [
      'title.required'  => 'Please provide a navigation menu title',
      'link.url'  => 'Please provide a valid URL for link',
    ]);

    $navigation_menu = new NavigationMenu();
    $navigation_menu->title = $request->title;
    $navigation_menu->link = $request->link;
    $navigation_menu->save();

    session()->flash('success', 'A new navigation menu has added successfully !!');
    return redirect()->route('admin.navigation_menus');

  }

  public function edit($id)
  {
    $navigation_menu= NavigationMenu::find($id);
    if (!is_null($navigation_menu)) {
      return view('backend.pages.navigation_menus.edit', compact('navigation_menu'));
    }else {
      return resirect()->route('admin.navigation_menus');
    }
  }


  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'title'  => 'required',
      'link'  => 'nullable|url',
    ],
    [
      'title.required'  => 'Please provide a navigation menu title',
      'link.url'  => 'Please provide a valid URL for link',
    ]);

    $navigation_menu = NavigationMenu::find($id);
    $navigation_menu->title = $request->title;
    $navigation_menu->link = $request->link;
    $navigation_menu->save();

    session()->flash('success', 'NavigationMenu has updated successfully !!');
    return redirect()->route('admin.navigation_menus');

  }

  public function delete($id)
  {
    $navigation_menu = NavigationMenu::find($id);
    if (!is_null($navigation_menu)) {
      $navigation_menu->delete();
    }
    session()->flash('success', 'NavigationMenu has deleted successfully !!');
    return back();

  }
}
