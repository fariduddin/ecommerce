<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Hash;

class PagesController extends Controller
{

  // public function __construct(){
  //   $this->middleware('auth:admin');
  // }


  /**
  * [return to index page & with count of some model]
  * @return [view] [backend.pages.index]
  */
  public function index()
  {
    
    return view('backend.pages.index');
  }

  /**
  * [retrun to changePasswordForm]
  * @return [type] [description]
  */
  public function changePasswordForm()
  {
    return view('backend.pages.changePassword');
  }


  /**
  * [change password if password match]
  * @param  Request $request [form data]
  * @return [type]           [description]
  */
  public function changePassword(Request $request)
  {
    $this->validate($request, [
      'old_password' => 'required',
      'new_password' => 'required',
      'confirm_new_password' => 'required',
    ]);

    if($request->new_password == $request->confirm_new_password){
      $admin = Admin::find(Auth::guard('admin')->user()->id);
      if(Hash::check($request->old_password,$admin->password)){
        $admin->password = bcrypt($request->new_password);
        $admin->save();
        session()->flash('success', 'Password changed successfully');
        return redirect()->route('admin.dashboard');
      }
      else{
        session()->flash('old_password', 'Old password does not matched');
        return back();
      }
    }
    else{
      session()->flash('confirm_password', 'Confrim password does not matched');
      return back();
    }
  }
}
