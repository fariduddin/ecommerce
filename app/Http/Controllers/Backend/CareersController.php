<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Career;
use File;

class CareersController extends Controller
{

  // public function __construct(){
  //   $this->middleware('auth:admin');
  // }

  public function index()
  {
    $careers = Career::orderBy('id', 'desc')->orderBy('status', 0)->get();
    return view('backend.pages.careers.index', compact('careers'));
  }

  public function destroy($id)
  {
    $career = Career::find($id);
    if (!is_null($career)) {

      // Delete category image
      if (File::exists('files/career-cv/'.$career->cv)) {
        File::delete('files/career-cv/'.$career->cv);
      }
      $career->delete();
    }
    session()->flash('success', 'Career has deleted successfully !!');
    return back();

  }

}
