<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Helpers\ImageUploadHelper;
use App\Helpers\UploadHelper;
use App\Helpers\StringHelper;
use App\Models\Admin;
use Auth;
use Image;

class AdminsController extends Controller
{

  // public function __construct(){
  //   $this->middleware('auth:admin');
  // }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    if (Auth::guard('admin')->user()->type == "Super Admin") {
      $admins = Admin::orderBy('name', 'asc')->get();
      return view('backend.pages.admins.index', compact('admins'));
    }else {
      session()->flash('error', 'Sorry !! You are not authorized to view this !!');
      return redirect()->route('admin.index');
    }
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    if (Auth::guard('admin')->user()->type == "Super Admin") {
      return view('backend.pages.admins.create');
    }else {
      session()->flash('error', 'Sorry !! You are not authorized to view this !!');
      return redirect()->route('admin.index');
    }
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    if (Auth::guard('admin')->user()->type == "Super Admin") {
      $admin = new Admin();

      $this->validate($request, [
        'name' => 'required',
        'phone_no' => 'nullable',
        'mobile_no' => 'nullable',
        'email' => 'required|email|unique:admins',
        'image' => 'nullable|image',
        'password' => 'required|min:6|confirmed',
      ]);

      $admin->name = $request->name;
      $admin->phone_no = $request->phone_no;
      $admin->mobile_no = $request->mobile_no;
      $admin->username= StringHelper::createSlug($request->name, 'Admin', 'username', '');
      $admin->email = $request->email;
      $admin->type = 'Admin';
      $admin->password = Hash::make($request->password);

      // Image Upload
      $admin->image = ImageUploadHelper::upload('image', $request->file('image'), time(), 'images/admins');
      
 /*      if (count($request->image) > 0) {
	      $image = $request->file('image');
	      $img = time() . '.'. $image->getClientOriginalExtension();
	      $location = 'images/admins/' .$img;
	      Image::make($image)->save($location);
	      $admin->image = $img;
	}
    */

      $admin->save();

      session()->flash('success', 'New Admin has been added successfully !!');
      return redirect()->route('admin.getAdmins');
    }else {
      session()->flash('error', 'Sorry !! You are not authorized to view this !!');
      return redirect()->route('admin.index');
    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    if (Auth::guard('admin')->user()->type == "Super Admin") {

      $admin = Admin::find($id);

      if (!is_null($admin)) {
        return view('backend.pages.admins.show', compact('admin'));
      }
      session()->flash('error', 'Sorry !! The admin is not available');
      return redirect()->route('admin.getAdmins');
    }else {
      session()->flash('error', 'Sorry !! You are not authorized to view this !!');
      return redirect()->route('admin.index');
    }
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    if (Auth::guard('admin')->user()->type == "Super Admin") {

      $admin = Admin::find($id);

      if (!is_null($admin)) {
        return view('backend.pages.admins.edit', compact('admin'));
      }
      session()->flash('error', 'Sorry !! The admin is not available');
      return redirect()->route('admin.getAdmins');
    }else {
      session()->flash('error', 'Sorry !! You are not authorized to view this !!');
      return redirect()->route('admin.index');
    }
  }

  /**
  * Logged In Admin Can Change his profile information
  */
  public function myProfile()
  {
    $admin = Admin::find(Auth::guard('admin')->user()->id);

    if (!is_null($admin)) {
      return view('backend.pages.admins.my-profile', compact('admin'));
    }
    session()->flash('error', 'Sorry !! The admin is not available');
    return redirect()->route('admin.getAdmins');
  }



  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    if (Auth::guard('admin')->user()->type == "Super Admin") {

      $admin = Admin::find($id);

      $this->validate($request, [
        'name' => 'required',
        'phone_no' => 'nullable',
        'mobile_no' => 'nullable',
        'email' => 'required|email|unique:admins,email,'.$admin->id,
        'image' => 'nullable|image',
        'password' => 'nullable|min:6|confirmed',
      ]);

      $admin->name = $request->name;
      $admin->phone_no = $request->phone_no;
      $admin->mobile_no = $request->mobile_no;
      $admin->email = $request->email;

      if ($request->password) {
        $admin->password = Hash::make($request->password);
      }
      
      // Image Upload
      if($request->image){
        // if this row has image then update it else upload image
        $imageName = time();
        if($admin->image){
          $admin->image = ImageUploadHelper::updateImage('image', $request->file('image'), $imageName, 'images/admins', $admin->image);
        }
        // upload image
        else{
          $admin->image = ImageUploadHelper::upload('image', $request->file('image'), $imageName, 'images/admins');
        }
      }
      $admin->save();

      session()->flash('success', 'Admin has been updated successfully !!');
      return redirect()->route('admin.getAdmins');
    }else {
      session()->flash('error', 'Sorry !! You are not authorized to view this !!');
      return redirect()->route('admin.index');
    }
  }

  /**
  * Logged In Admins Profile Update
  */
  public function myProfileUpdate(Request $request)
  {
    $admin = Admin::find(Auth::guard('admin')->user()->id);

    $this->validate($request, [
      'name' => 'required',
      'phone_no' => 'nullable',
      'mobile_no' => 'nullable',
      'email' => 'required|email|unique:admins,email,'.$admin->id,
      'image' => 'nullable|image',
      'password' => 'nullable|min:6|confirmed',
    ]);

    $admin->name = $request->name;
    $admin->phone_no = $request->phone_no;
    $admin->mobile_no = $request->mobile_no;
    $admin->email = $request->email;

    if ($request->password) {
      $admin->password = Hash::make($request->password);
    }
    // Image Upload
    if($request->image){
      // if this row has image then update it else upload image
      $imageName = time();
      if($admin->image){
        $admin->image = ImageUploadHelper::updateImage('image', $request->file('image'), $imageName, 'images/admins', $admin->image);
      }
      // upload image
      else{
        $admin->image = ImageUploadHelper::upload('image', $request->file('image'), $imageName, 'images/admins');
      }
    }
    $admin->save();

    session()->flash('success', 'Your profile has been updated successfully !!');
    return back();
  }


  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    if (Auth::guard('admin')->user()->type == "Super Admin") {
      $admin = Admin::find($id);
      if(!is_null($admin->image)){
        UploadHelper::deleteFile('images/admins/'.$admin->image);
      }
      $admin->delete();

      session()->flash('error', 'Admin information has been deleted successfully');
      return redirect()->route('admin.getAdmins');
    }else {
      session()->flash('error', 'Sorry !! You are not authorized to view this !!');
      return redirect()->route('admin.index');
    }
  }
}
