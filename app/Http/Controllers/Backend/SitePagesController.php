<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Hash;

use App\Helpers\StringHelper;
use App\Models\Page;

class SitePagesController extends Controller
{

  // public function __construct(){
  //   $this->middleware('auth:admin');
  // }


  /**
  * [return to index page & with count of some model]
  * @return [view] [backend.pages.index]
  */
  public function index()
  {
    $pages = Page::orderBy('id', 'desc')->get();
    return view('backend.pages.site_pages.index', compact('pages'));
  }


    public function create()
    {
      return view('backend.pages.site_pages.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
        'title'  => 'required',
        'description'  => 'required'
      ],
      [
        'title.required'  => 'Please provide a page title',
        'description.required'  => 'Please provide a page description',
      ]);

      $page = new Page();
      $page->title = $request->title;
      $page->slug = StringHelper::createSlug($request->title, 'Page', 'slug');
      $page->description = $request->description;
      $page->save();

      session()->flash('success', 'A new page has added successfully !!');
      return redirect()->route('admin.pages');

    }

    public function edit($id)
    {
      $page= Page::find($id);
      if (!is_null($page)) {
        return view('backend.pages.site_pages.edit', compact('page'));
      }else {
        return resirect()->route('admin.pages');
      }
    }


    public function update(Request $request, $id)
    {
      $page = Page::find($id);

      $this->validate($request, [
        'title'  => 'required',
        'slug'  => 'required|alpha-dash|unique:pages,slug,'.$page->id,
        'description'  => 'required',
      ],
      [
        'title.required'  => 'Please provide a page title',
        'description.required'  => 'Please provide a page description',
        'slug.unique'  => 'Please provide a unique slug to generate a real URL for that page link',
      ]);


      $page->title = $request->title;
      $page->description = $request->description;
      $page->slug = $request->slug;
      $page->save();

      session()->flash('success', 'Page has updated successfully !!');
      return redirect()->route('admin.pages');

    }

    public function delete($id)
    {
      $page = Page::find($id);
      if (!is_null($page)) {
        $page->delete();
      }
      session()->flash('success', 'Page has deleted successfully !!');
      return back();

    }

}
