<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Review;
use Auth;

class ReviewController extends Controller
{

  // public function __construct(){
  //   $this->middleware('auth:admin');
  // }

  public function index()
  {
    $reviews = Review::orderBy('id', 'desc')->get();
    return view('backend.pages.reviews.index', compact('reviews'));
  }

  public function change_status(Request $request, $id)
  {
    $review = Review::find($id);
    if ($review->status == 1) {
      $review->status = 2;
      $message = 'Review has hidden !!';
    }else {
      $review->status = 1;
      $message = 'Review has published !!';
    }

    $review->save();
    session()->flash('success', $message);
    return back();
  }

  public function delete($id)
  {
    $review = Review::find($id);
    if ($review) {
      $review->delete();
      session()->flash('success', "Review has deleted !!");
    }else {
      session()->flash('sticky_error', "There is no review by this ID !!");
    }
    return back();
  }
}
