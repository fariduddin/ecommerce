<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Setting;

class SettingController extends Controller
{

  // public function __construct(){
  //   $this->middleware('auth:admin');
  // }

  public function index()
  {
    $setting = Setting::first();
    if (!is_null($setting)) {
      return view('backend.pages.settings', compact('setting'));
    }else {
      $setting = new Setting();
      $setting->site_title = "Priyo Fashion";
      return view('backend.pages.settings', compact('setting'));
    }
  }


  public function update(Request $request)
  {
    $this->validate($request, [
      'site_title'  => 'required'
    ]);

    $setting = Setting::first();
    $setting->site_title = $request->site_title;
    $setting->site_description = $request->site_description;
    $setting->shipping_cost_inside_dhaka = $request->shipping_cost_inside_dhaka;
    $setting->shipping_cost_outside_dhaka = $request->shipping_cost_outside_dhaka;
    $setting->phone_no = $request->phone_no;
    $setting->email = $request->email;
    $setting->site_location = $request->site_location;
    $setting->site_meta_keywords = $request->site_meta_keywords;
    $setting->site_meta_description = $request->site_meta_description;
    $setting->site_location_latitude = $request->site_location_latitude;
    $setting->site_location_longitude = $request->site_location_longitude;

    $setting->facebook_link = $request->facebook_link;
    $setting->twitter_link = $request->twitter_link;
    $setting->youtube_link = $request->youtube_link;
    $setting->pinterest_link = $request->pinterest_link;
    $setting->google_plus_link = $request->google_plus_link;
    $setting->linked_in_link = $request->linked_in_link;
    $setting->instagram_link = $request->instagram_link;

    $setting->save();
    session()->flash('success', 'Site settings has updated successfully !!');
    return back();
  }

}
