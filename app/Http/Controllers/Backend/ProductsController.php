<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Helpers\StringHelper;

use App\Models\Product;
use App\Models\Featured;
use App\Models\ProductImage;
use App\Models\ProductColor;
use App\Models\ProductSize;
use Image;
use Auth;
use File;

class ProductsController extends Controller
{

  // public function __construct(){
  //   $this->middleware('auth:admin');
  // }

  public function index()
  {
    $products = Product::orderBy('id', 'desc')->get();
    return view('backend.pages.products.index')->with('products', $products);
  }

  public function create()
  {
    return view('backend.pages.products.create');
  }

  public function edit($id)
  {
    $product = Product::find($id);
    return view('backend.pages.products.edit')->with('product', $product);
  }


  public function store(Request $request)
  {

    $request->validate([
      'title'         => 'required|max:150',
      'description'     => 'required',
      'price'             => 'required|numeric',
      'quantity'             => 'required|numeric',
      'category_id'             => 'required|numeric',
      'brand_id'             => 'required|numeric',
    ]);

    $product = new Product();

    $product->title = $request->title;
    $product->description = $request->description;
    $product->price = $request->price;
    $product->quantity = $request->quantity;

    $product->slug = StringHelper::createSlug($request->title, 'Product', 'slug');
    $product->category_id = $request->category_id;
    $product->brand_id = $request->brand_id;
    $product->admin_id = Auth::guard('admin')->user()->id;


    // Add optional Fields
    $product->offer_discount = $request->offer_discount;
    $product->offer_last_date = $request->offer_last_date;
    $product->sku = $request->sku;
    $product->fabrics = $request->fabrics;
    $product->cut = $request->cut;
    $product->sleeve = $request->sleeve;
    $product->collar = $request->collar;
    $product->length = $request->length;
    $product->occasion = $request->occasion;
    $product->event = $request->event;
    $product->value_addition = $request->value_addition;
    $product->washcare = $request->washcare;
    $product->meta_keywords = $request->meta_keywords;
    $product->meta_description = $request->meta_description;

    $product->save();

    if (count($files = $request->file('product_image')) > 0) {
      $i = 0;
      foreach ($files as $image) {
        $img = time().$i.'.'. $image->getClientOriginalExtension();
        $location = 'images/products/' .$img;
        Image::make($image)->save($location);

        $product_image = new ProductImage;
        $product_image->product_id = $product->id;
        $product_image->image = $img;
        $product_image->save();

        $i++;
      }
    }


    //ProductColor Model insert
    if (count($request->product_colors) > 0) {
      foreach ($request->product_colors as $color) {
        $product_color = new ProductColor();
        $product_color->product_id = $product->id;
        $product_color->color_id = $color;
        $product_color->save();
      }
    }

    //ProductSize Model insert
    if (count($request->product_sizes) > 0) {
      foreach ($request->product_sizes as $size) {
        if ($size != NULL) {
          $product_size = new ProductSize();
          $product_size->product_id = $product->id;
          $product_size->size = $size;
          $product_size->save();
        }
      }
    }


    session()->flash('success', 'Product has added successfully');
    return redirect()->route('admin.products');
  }
  public function update(Request $request, $id)
  {
    $product = Product::find($id);

    $request->validate([
      'title'         => 'required|max:150',
      'description'     => 'required',
      'price'             => 'required|numeric',
      'quantity'             => 'required|numeric',
      'category_id'             => 'required|numeric',
      'brand_id'             => 'required|numeric',
    ]);

    $product->title = $request->title;
    $product->description = $request->description;
    $product->price = $request->price;
    $product->quantity = $request->quantity;
    
    //if($request->title != $product->title){
    	//$product->slug = StringHelper::createSlug($request->title, 'Product', 'slug');
    //}

    
    $product->category_id = $request->category_id;
    $product->brand_id = $request->brand_id;
    $product->admin_id = Auth::guard('admin')->user()->id;


    // Add optional Fields
    $product->offer_discount = $request->offer_discount;
    $product->offer_last_date = $request->offer_last_date;
    $product->sku = $request->sku;
    $product->fabrics = $request->fabrics;
    $product->cut = $request->cut;
    $product->sleeve = $request->sleeve;
    $product->collar = $request->collar;
    $product->length = $request->length;
    $product->occasion = $request->occasion;
    $product->event = $request->event;
    $product->value_addition = $request->value_addition;
    $product->washcare = $request->washcare;
    $product->meta_keywords = $request->meta_keywords;
    $product->meta_description = $request->meta_description;

    $product->save();

    if (count($files = $request->file('product_image')) > 0) {
      $i = 0;
      foreach ($files as $image) {
        $img = time().$i.'.'. $image->getClientOriginalExtension();
        $location = 'images/products/' .$img;
        Image::make($image)->save($location);

        $product_image = new ProductImage;
        $product_image->product_id = $product->id;
        $product_image->image = $img;
        $product_image->save();

        $i++;
      }
    }

    //ProductColor Model insert
    if (count($request->product_colors) > 0) {
      //Delete all colors first of that product
      DB::table('product_colors')->where('product_id', $product->id)->delete();
      // if (!is_null($colors)) {
      //   $colors->delete();
      // }
      foreach ($request->product_colors as $color) {

        $product_color = new ProductColor();
        $product_color->product_id = $product->id;
        $product_color->color_id = $color;
        $product_color->save();
      }
    }

    //ProductSize Model insert
    if (count($request->product_sizes) > 0) {
      foreach ($request->product_sizes as $size) {
        if ($size != NULL) {
          if ($product->hasNotSize($size)) {
            $product_size = new ProductSize();
            $product_size->product_id = $product->id;
            $product_size->size = $size;
            $product_size->save();
          }

        }
      }
    }

    session()->flash('success', 'Product has updated successfully');
    return redirect()->route('admin.products');
  }


  public function delete($id)
  {
    $product = Product::find($id);
    if (!is_null($product)) {

      //Delete All Images
      $product_images = ProductImage::where('product_id', $id)->get();
      if (!is_null($product_images)) {
        // Delete
        foreach ($product_images as $image) {
          //Delete from folder
          if (File::exists('images/products/'.$image->image)) {
            File::delete('images/products/'.$image->image);
          }
          $image->delete();
        }
      }

      //Delete All Sizes
      $product_sizes = DB::table('product_sizes')->where('product_id', $id)->delete();

      //Delete all colors of that product
      $product_colors = DB::table('product_colors')->where('product_id', $id)->delete();

      //Delete all featured products of that product
      $product_colors = DB::table('featureds')->where('product_id', $id)->delete();

      //Delete all cart item products of that product
      $product_colors = DB::table('carts')->where('product_id', $id)->delete();

      // Delete all cart of that product
      $product->delete();
    }
    session()->flash('success', 'Product has deleted successfully !!');
    return back();

  }

  public function deleteImage($id, Request $request)
  {
    $product_image = ProductImage::find($request->image_id);
    if (!is_null($product_image)) {
      $product_image->delete();
    }
    session()->flash('success', 'Product Image has deleted successfully !!');
    return back();

  }

  public function addFeatured($id)
  {
    $featured = Featured::where('product_id', $id)->first();

    if (!is_null($featured)) {
      $featured->delete();
      session()->flash('success', 'Product has deleted from featured product List');
      return back();
    }

    $featured = new Featured();
    $featured->product_id = $id;
    $featured->save();

    session()->flash('success', 'Product has added to featured product successfully !!');
    return back();

  }
}
