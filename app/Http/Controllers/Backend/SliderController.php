<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ImageUploadHelper;
use App\Helpers\StringHelper;
use App\Models\Slider;
use Session;

class SliderController extends Controller
{
  // public function __construct(){
  //   $this->middleware('auth:admin');
  // }

  /**
  * [retrieve slider inforation & return to slider index page]
  * @return [view] [backend.pages.sliders.index]
  */
  public function index()
  {
    $sliders = Slider::orderBy('priority', 'desc')->get();
    return view('backend.pages.sliders.index', compact('sliders'));
  }


  /**
  * [store silder information]
  * @param  Request $request [form data]
  * @return [type]           [description]
  */
  public function store(Request $request)
  {
    $this->validate($request, [
      'text' => 'required|max:190',
      'image' => 'required',
      'priority' => 'nullable|numeric',
      'button_link' => 'nullable|url',
    ]);


    $imageName = time();
    $slider = new Slider();
    if($request->priority!= null && $request->priority != ''){
      $slider->priority = $request->priority;
    }
    $slider->text = $request->text;
    $slider->button_link = $request->button_link;
    $slider->image = ImageUploadHelper::upload('image', $request->file('image'), $imageName, 'images/slider');
    $slider->save();

    session()->flash('success', 'Slider image added successfully');
    return redirect()->route('admin.sliders.index');
  }


  /**
  * [update slider information]
  * @param  Request $request [form data]
  * @param  [type]  $id      [slider->id]
  * @return [type]           [description]
  */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'text' => 'required|max:190',
      'button_link' => 'nullable|url',
    ]);

    $slider = Slider::find($id);
    $slider->text = $request->text;
    $perviousImage = $slider->image;
    if($request->priority != null && $request->priority != ''){
      $slider->priority = $request->priority;
    }
    $slider->button_link = $request->button_link;

    // check if image request
    if($request->image){
      // if this row has image then update it else upload image
      $imageName = time();
      if($slider->image){
        $slider->image = ImageUploadHelper::updateImage('image', $request->file('image'), $imageName, 'images/slider', $slider->image);
      }
      // upload image
      else{
        $slider->image = ImageUploadHelper::upload('image', $request->file('image'), $imageName, 'images/slider');
      }
    }

    $slider->save();

    session()->flash('success', 'Slider image updated successfully');
    return redirect()->route('admin.sliders.index');
  }


  /**
  * [delete slider information with file(image)]
  * @param  [type] $id [slider->id]
  * @return [type]     [description]
  */
  public function destroy($id)
  {
    $slider = Slider::find($id);
    if(!is_null($slider->image)){
      ImageUploadHelper::deleteImage('images/slider/'.$slider->image);
    }
    $slider->delete();
    session()->flash('error', 'Slider image deleted successfully');
    return redirect()->route('admin.sliders.index');
  }
}
