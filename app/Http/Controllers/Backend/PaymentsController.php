<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Payment;
use Image;
use File;

class PaymentsController extends Controller
{

  // public function __construct(){
  //   $this->middleware('auth:admin');
  // }

  public function index()
  {
    $payments = Payment::orderBy('priority', 'asc')->get();
    return view('backend.pages.payments.index', compact('payments'));
  }

  public function create()
  {
    return view('backend.pages.payments.create');
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'name'  => 'required',
      'short_name'  => 'required|unique:payments',
      'no'  => 'nullable',
      'type'  => 'nullable',
      'priority'  => 'required|numeric',
      'image'  => 'nullable|image'
    ],
    [
      'name.required'  => 'Please provide a payment name',
      'short_name.required'  => 'Please provide a payment short name',
      'image.image'  => 'Please provide a valid image with .jpg, .png, .gif, .jpeg exrension..',
    ]);

    $payment = new Payment();
    $payment->name = $request->name;
    $payment->short_name = $request->short_name;
    $payment->no = $request->no;
    $payment->type = $request->type;
    $payment->priority = $request->priority;

    //insert images also
    if (count($request->image) > 0) {
      $image = $request->file('image');
      $img = time() . '.'. $image->getClientOriginalExtension();
      $location = 'images/payments/' .$img;
      Image::make($image)->save($location);
      $payment->image = $img;
    }
    $payment->save();

    session()->flash('success', 'A new payment has added successfully !!');
    return redirect()->route('admin.payments');

  }

  public function edit($id)
  {
    $payment= Payment::find($id);
    if (!is_null($payment)) {
      return view('backend.pages.payments.edit', compact('payment'));
    }else {
      return resirect()->route('admin.payments');
    }
  }


  public function update(Request $request, $id)
  {
    $payment = Payment::find($id);

    $this->validate($request, [
      'name'  => 'required',
      'short_name'  => 'required|unique:payments,short_name,'.$payment->id,
      'no'  => 'nullable',
      'type'  => 'nullable',
      'priority'  => 'required|numeric',
      'image'  => 'nullable|image'
    ],
    [
      'name.required'  => 'Please provide a payment name',
      'short_name.required'  => 'Please provide a payment short name',
      'image.image'  => 'Please provide a valid image with .jpg, .png, .gif, .jpeg exrension..',
    ]);

    $payment->name = $request->name;
    $payment->short_name = $request->short_name;
    $payment->no = $request->no;
    $payment->type = $request->type;
    $payment->priority = $request->priority;

    //insert images also
    if (count($request->image) > 0) {
      //Delete the old image from folder

      if (File::exists('images/payments/'.$payment->image)) {
        File::delete('images/payments/'.$payment->image);
      }

      $image = $request->file('image');
      $img = time() . '.'. $image->getClientOriginalExtension();
      $location = 'images/payments/' .$img;
      Image::make($image)->save($location);
      $payment->image = $img;
    }
    $payment->save();

    session()->flash('success', 'Payment has updated successfully !!');
    return redirect()->route('admin.payments');

  }

  public function delete($id)
  {
    $payment = Payment::find($id);
    if (!is_null($payment)) {
      // Delete payment image
      if (File::exists('images/payments/'.$payment->image)) {
        File::delete('images/payments/'.$payment->image);
      }
      $payment->delete();
    }
    session()->flash('success', 'Payment has deleted successfully !!');
    return back();

  }
}
