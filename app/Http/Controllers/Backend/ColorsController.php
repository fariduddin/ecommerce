<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Color;

class ColorsController extends Controller
{

  // public function __construct(){
  //   $this->middleware('auth:admin');
  // }

  public function index()
  {
    $colors = Color::orderBy('id', 'desc')->get();
    return view('backend.pages.colors.index', compact('colors'));
  }

  public function create()
  {
    return view('backend.pages.colors.create');
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'name'  => 'required|unique:colors',
      'code'  => 'required',
    ],
    [
      'name.required'  => 'Please provide a color name',
      'code.required'  => 'Please provide a valid color code',
    ]);

    $color = new Color();
    $color->name = $request->name;
    $color->code = $request->code;
    $color->save();

    session()->flash('success', 'A new color has added successfully !!');
    return redirect()->route('admin.colors');

  }

  public function edit($id)
  {
    $color= Color::find($id);
    if (!is_null($color)) {
      return view('backend.pages.colors.edit', compact('color'));
    }else {
      return resirect()->route('admin.colors');
    }
  }


  public function update(Request $request, $id)
  {
    $color = Color::find($id);

    $this->validate($request, [
      'name'  => 'required|unique:colors,name,'.$color->id,
      'code'  => 'required',
    ],
    [
      'name.required'  => 'Please provide a color name',
      'code.required'  => 'Please provide a valid color code',
    ]);

    $color->name = $request->name;
    $color->code = $request->code;
    $color->save();

    session()->flash('success', 'Color has updated successfully !!');
    return redirect()->route('admin.colors');

  }

  public function delete($id)
  {
    $color = Color::find($id);
    if (!is_null($color)) {
      $color->delete();
    }
    session()->flash('success', 'Color has deleted successfully !!');
    return back();

  }
}
