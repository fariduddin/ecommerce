<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use App\Http\Controllers\Controller;

use App\Notifications\OrderCompleted;
use App\Models\Order;

class OrderController extends Controller
{

  // public function __construct(){
  //   $this->middleware('auth:admin');
  // }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $orders = Order::orderBy('id', 'desc')->where('is_seen_by_admin', 0)->get();
    return view('backend.pages.orders.index', compact('orders'));
  }


  public function all()
  {
    $orders = Order::orderBy('id', 'desc')->get();
    return view('backend.pages.orders.index', compact('orders'));
  }


  public function seen()
  {
    $orders = Order::orderBy('id', 'desc')->where('is_seen_by_admin', 1)->get();
    return view('backend.pages.orders.index', compact('orders'));
  }

  public function completed()
  {
    $orders = Order::orderBy('id', 'desc')->where('is_completed', 1)->get();
    return view('backend.pages.orders.index', compact('orders'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    $order = Order::find($id);
    if (!is_null($order)) {
      $order->is_seen_by_admin = 1;
      $order->save();
      return view('backend.pages.orders.show', compact('order'));
    }else {
      session()->flash('message', 'Sorry !! There is no order by this id');
      return redirect()->route('admin.orders.index');
    }
  }


  public function makePaid($id)
  {
    $order = Order::find($id);

    if (!is_null($order)) {
      if ($order->is_paid) {
        $order->is_paid = 0;
      }else {

        $order->is_paid = 1;

      }
      $order->save();

      session()->flash('success', 'Order has set as a paid order !!');
      return back();
    }else {
      session()->flash('message', 'Sorry !! There is no order by this id');
      return redirect()->route('admin.orders.index');
    }
  }

  public function makeCompleted($id)
  {
    $order = Order::find($id);

    if (!is_null($order)) {
      if ($order->is_completed) {
        $order->is_completed = 0;
        $message = "Order has removed from completed order";
      }else {
        $order->is_completed = 1;
        // if ($order->email != NULL) {
        //   // Send an email to the user
        //   Notification::route('mail', $order->email)
        //     // ->route('nexmo', '5555555555')
        //     ->notify(new OrderCompleted($order));
        //     $message = "Order has set as a completed order and an email has sent to user also !!";
        // }
        
        // Get all cart items in this order and get the number of item and product quantity removes that quantity
        
        foreach ($order->carts as $item){
        	$product = $item->product;
        	$quantity = $item->product_quantity;
        	$product->quantity -= $quantity;
        	$product->save();
        }

        
        $message = "Order has set as a completed order !!";

      }
      $order->save();
      session()->flash('message', $message);
      return back();
    }else {
      session()->flash('message', 'Sorry !! There is no order by this id');
      return redirect()->route('admin.orders.index');
    }
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $order = Order::find($id);
    if (!is_null($order)) {
      $order->delete();
    }

    return back();
  }
}
