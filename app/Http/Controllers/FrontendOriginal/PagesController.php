<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Featured;
use App\Models\Page;
use App\Models\Product;
use Illuminate\Http\Request;

class PagesController extends Controller
{


  //   public function __construct()
  // {
  //   $this->middleware('auth');
  // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $latest_products = Product::orderBy('id', 'desc')->limit(12)->get();
        $most_viewed_products = Product::orderBy('total_view', 'desc')->limit(12)->get();
        $featured_products = Featured::orderBy('id', 'desc')->get();
        return view('frontend.pages.index', compact('latest_products', 'most_viewed_products', 'featured_products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $page = Page::where('slug', $slug)->first();
    if (!is_null($page)) {
      return view('frontend.pages.show', compact('page'));
    }else {
      session()->flash('message', 'Sorry, the page has not available in the site');
      return redirect()->route('index');
    }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
