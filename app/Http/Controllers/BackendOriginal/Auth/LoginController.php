<?php

namespace App\Http\Controllers\Backend\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Session;

class LoginController extends Controller
{

    // use AuthenticatesUsers;

    protected $redirectTo = '/admin';

    public function __construct(){
        $this->middleware('guest:admin', ['except' => ['logout']]);
    }

    public function showLoginForm()
    {
        if (Auth::check()) {
            return redirect('/admin');
        }
        return view('backend.auth.login');
    }

    public function login(Request $request)
    {

        //Validate the form data
        $request->validate([
            'email'         => 'required',
            'password'         => 'required|min:6'
        ]);

        //Attempt to log the employee in
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            //If successful then redirect to the intended location
            session()->flash('login_success', 'Successfully Logged In');
            return redirect()->intended(route('admin.index'));
        } else {
            if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
                //If successful then redirect to the intended location
                session()->flash('login_success', 'Successfully Logged In');
                return redirect()->intended(route('admin.index'));
            } else {
                //If unsuccessfull, then redirect to the admin login with the data
                Session::flash('login_error', "email and password Information doesn't match");
                return redirect()->back()->withInput($request->only('email', 'remember'));
            }
        }
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/admin');
    }
    // public function __construct()
    // {
    //     $this->middleware('guest')->except('logout');
    // }
}
