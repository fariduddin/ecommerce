<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\StringHelper;
use App\Helpers\UploadHelper;
use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();
        return view('backend.pages.department.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required |unique:departments'
        ]);
        $post = new Department();
        $post->name = $request->name;
        $post->description = $request->description;
        $post->is_active =1;
        $post->slug = StringHelper::createSlug($request->name, 'Category', 'slug', '-');
        $post->image = UploadHelper::upload('image', $request->file('image'), Str::slug($request->name, '-') . '-' . time(), 'public/images/post_images');
        $post->ip_address = $request->ip();
        $post->save();
        $request->session()->flash('success', "Department Create successfully");
        return redirect()->route('department.create');

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $department = Department::find($id);
       return view('backend.pages.department.edit',compact('department'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $department = Department::find($id);
        $department->name = $request->name;
        $department->description = $request->description;
        $department->is_active =1;
        $department->ip_address = $request->ip();
        $department->save();
        $request->session()->flash('success', "Department Delete successfully");
        return redirect()->route('department.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $flight = Department::find($id);
        $flight->delete();
        $request->session()->flash('success', "Department Delete successfully");
        return redirect()->route('department.index');
    }
}
