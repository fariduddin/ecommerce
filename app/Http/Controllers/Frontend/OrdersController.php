<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Order;
use App\Models\Payment;
use App\Models\OrderPayment;
use App\Models\Cart;

use Auth;

class OrdersController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    //
  }


  public function checkout()
  {
    return view('frontend.pages.checkouts');
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $this->validate($request, [
      'name'  => 'bail|required|string',
      'email'  => 'nullable|email',
      'phone'  => 'required',
      'shipping_address'  => 'required',
      'payments'  => 'required'
    ]);

    $order = new Order();
    $order->name = $request->name;
    $order->email = $request->email;
    $order->phone = $request->phone;
    $order->message = $request->message;
    $order->shipping_address = $request->shipping_address;
    if (Auth::check()) {
      $order->user_id = Auth::id();
    }
    $order->ip_address = request()->ip();

    // $order->payments = $request->payments;

    if ($request->payments != 'cash_in') {
      if ($request->transaction_id == NULL) {
        session()->flash('sticky_error', 'For Bkash and Rocket Payment, please give the transaction ID after sending the specific amount to that account. !!');
        return back();
      }
    }

    $order->save();

    // Add Cart Items's to order_id = $order->id
    $total_items = Cart::total_items();
    foreach ($total_items as $item) {
      $item->order_id = $order->id;
      $item->save();
    }

    // Add Order Payment Method Model
    $order_payment = new OrderPayment();
    $order_payment->order_id = $order->id;

    if ($request->transaction_id != NULL) {
      $order_payment->transaction_id = $request->transaction_id;
    }
    // Get payment id
    $payment_id = Payment::where('short_name', $request->payments)->first()->id;
    if (!is_null($payment_id)) {
      $order_payment->payment_id = $payment_id;
    }else {
      session()->flash('sticky_error', 'For Bkash and Rocket Payment, please give the transaction ID after sending the specific amount to that account. !!');
      return back();
    }
    $order_payment->save();
    session()->flash('success', 'Your order has stored successfully !! Wait and soon you will receive a confirmation email or phone message from us about your order.');
    
    if(Auth::check('web')){
    	return redirect()->route('users.orders', Auth::user());
    }
    return redirect()->route('index');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
}
