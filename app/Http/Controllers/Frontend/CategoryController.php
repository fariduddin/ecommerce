<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\Category;

class CategoryController extends Controller
{
  public function index()
  {
    $categories = Category::orderBy('name', 'asc')->get();
    return view('frontend.pages.products.categories.index', compact('categories'));
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $category = Category::where('slug', $slug)->first();
        if (!is_null($category)) {
          return view('frontend.pages.products.index', compact('category'));
        }else {
          return redirect()->route('products.index');
        }
    }

}
