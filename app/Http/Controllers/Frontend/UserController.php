<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Helpers\StringHelper;
use App\Models\Division;
use App\Models\District;

use Auth;

class UserController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }


  public function dashboard()
  {
    $user = Auth()->user();
    return view('frontend.pages.users.dashboard', compact('user'));
  }
  
  
  public function orders()
  {
    $user = Auth()->user();
    return view('frontend.pages.users.orders', compact('user'));
  }

  public function edit()
  {

    $divisions = Division::orderBy('priority', 'asc')->get();
    $districts = District::orderBy('name', 'asc')->get();

    $user = Auth()->user();
    return view('frontend.pages.users.edit', compact('user', 'divisions', 'districts'));
  }

  public function update(Request $request)
  {
    $user = Auth()->user();

    Validator::make($request->all(), [
      'first_name' => 'required|string|max:30',
      'last_name' => 'nullable|string|max:15',
      'email' => 'required|string|email|max:100|unique:users,email,'.$user->id,
      'password' => 'nullable|string|min:6|confirmed',
      'division_id' => 'required|numeric',
      'district_id' => 'required|numeric',
      'phone_no' => 'required|max:15',
      'street_address' => 'required|max:150',
    ],
    ['division_id.required'  => 'Please select your division',
    'district_id.required'  => 'Please select your district'
    ])->validate();


    $user->first_name = $request->first_name;
    $user->last_name = $request->last_name;
    $user->division_id = $request->division_id;
    $user->district_id = $request->district_id;
    $user->phone_no = $request->phone_no;
    $user->street_address = $request->street_address;
    $user->ip_address = $request->ip_address;
    $user->email = $request->email;
    $user->shipping_address = $request->shipping_address;

    if ($request->password != NULL) {
      $user->password = Hash::make($request->password);
    }
    $user->save();

    session()->flash('success', 'User Information has updated successfully !!');
    return back();
  }
}
