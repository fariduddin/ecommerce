<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\Featured;
use App\Models\ProductImage;
use App\Models\Category;
use App\Models\Page;
use App\Models\Brand;

class PagesController extends Controller
{
  public function index()
  {
    $latest_products = Product::orderBy('id', 'desc')->limit(12)->get();
    $most_viewed_products = Product::orderBy('total_view', 'desc')->limit(12)->get();
    $featured_products = Featured::orderBy('id', 'desc')->get();

    return view('frontend.pages.index', compact('latest_products', 'most_viewed_products', 'featured_products'));
  }

  public function show($slug)
  {
    $page = Page::where('slug', $slug)->first();
    if (!is_null($page)) {
      return view('frontend.pages.product', compact('page','featured_products'));
    }else {
      session()->flash('message', 'Sorry, the page has not available in the site');
      return redirect()->route('index');
    }
  }


}
