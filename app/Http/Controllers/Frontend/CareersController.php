<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Heleprs\ImageUploadHelper;
use App\Models\Career;

class CareersController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'   => 'required',
            'email'  => 'required',
            'phone'  => 'required',
            'cv'     => "required|mimes:pdf|max:10000"
        ]);
        $career = new Career();
        $career->name = $request->name;
        $career->email = $request->email;
        $career->phone = $request->phone;
        $file = $request->cv;

        if ($request->has('cv')) {
          $filename = time(). '.' .$file->getClientOriginalExtension();
          $extension = $file->getClientOriginalExtension();
          if ($extension == 'pdf' || $extension == 'PDF' || $extension == 'doc' || $extension == 'docs') {
            $file->move('files/career-cv', $filename);
          }
          $career->cv = $filename;
        }
        session()->flash('success', 'Thank you !! Your CV has granted successfully !! Soon admin will see it..');
        $career->save();
        return back();
    }

    public function show()
    {
      return view('frontend.pages.career');
    }

}
