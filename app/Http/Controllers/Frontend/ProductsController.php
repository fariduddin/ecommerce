<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Featured;
use App\Models\Product;
use App\Models\ProductSize;
use App\Models\ProductColor;
use App\Models\ProductImage;

class ProductsController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $products = Product::orderBy('id', 'desc')->paginate(20);
    return view('frontend.pages.products.index', compact('products'));
  }

  public function latest()
  {
    $products = Product::orderBy('id', 'desc')->paginate(20);
    return view('frontend.pages.products.index', compact('products'));
  }

  public function price_fliter_post(Request $request)
  {
    $this->validate($request, [
      'lower_limit'  => 'required',
      'upper_limit'  => 'required'
    ]);

    $products = Product::where('price', '>=', $request->lower_limit)->where('price', '<=', $request->upper_limit)->orderBy('id', 'desc')->paginate(20);
    if (is_null($products)) {
      session()->flash('sticky_error', 'Sorry !! There is no product for your query. Try Other Products');
    }
    return view('frontend.pages.products.index', compact('products'));
  }

  public function filter_product(Request $request)
  {

    // For Different size product
    $size = $request->size;
    $product_size = ProductSize::where('size', "$size")->get();
    $size_array = array();
    foreach($product_size as $value) {
      $size_array[] = $value->product_id;
    }
    $products_by_size = Product::whereIn('id', $size_array)->get();

    // For Different color product
    $color_id = $request->color_id;
    $product_color = ProductColor::where('color_id', "$color_id")->get();
    $collor_array = array();
    foreach($product_color as $value) {
      $collor_array[] = $value->product_id;
    }
    $products_by_color = Product::whereIn('id', $collor_array)->get();


    // For different Fabrics Products
    $fabrics = $request->fabrics;
    $products_by_fabrics = Product::where('fabrics', 'like', "%$fabrics%")->get();


    // Products by Price Filtering
    $price_id = $request->min_price;
    $price_id2 = $request->max_price;
    $price = DB::select("Select * from products WHERE price >= '$price_id' && price <= '$price_id2'");
    $price_array = array();
    foreach($price as $value) {
      $price_array[] = $value->id;
    }
    $products_by_price = Product::whereIn('id', $price_array)->get();

    return view('frontend.pages.products.full_search', compact('products_by_size', 'products_by_color', '$products_by_fabrics', 'products_by_price'));
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($slug)
  {
    $product = Product::where('slug', $slug)->first();
    $featured_products = Featured::orderBy('id', 'desc')->get();
    if (!is_null($product)) {
      $product->increment('total_view');
      return view('frontend.pages.products.show', compact('product','featured_products'));
    }else {
      session()->flash('message', 'The product you searched, could not found !!');
      return  redirect()->route('products.index');
    }
  }


  public function search(Request $request)
  {
    $search = $request->search;
    if ($search != NULL) {
      $products = Product::orWhere('title', 'like', '%'.$search.'%')
      ->orWhere('description', 'like', '%'.$search.'%')
      ->orWhere('quantity', 'like', '%'.$search.'%')
      ->orWhere('meta_description', 'like', '%'.$search.'%')
      ->orWhere('meta_keywords', 'like', '%'.$search.'%')
      ->orWhere('sku', 'like', '%'.$search.'%')
      ->orWhere('fabrics', 'like', '%'.$search.'%')
      ->orWhere('cut', 'like', '%'.$search.'%')
      ->orWhere('sleeve', 'like', '%'.$search.'%')
      ->orWhere('collar', 'like', '%'.$search.'%')
      ->orWhere('value_addition', 'like', '%'.$search.'%')
      ->orWhere('occasion', 'like', '%'.$search.'%')
      ->orWhere('event', 'like', '%'.$search.'%')
      ->paginate(20);
    }else {
      $products = Product::orderBy('id', 'desc')->paginate(20);
    }
    return view('frontend.pages.products.index', compact('products', 'search'));

  }

}
