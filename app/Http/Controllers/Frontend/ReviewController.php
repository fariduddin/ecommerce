<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Review;
use Auth;

class ReviewController extends Controller
{

  public function __construct(){
    $this->middleware('auth:web');
  }

  public function index()
  {
    $user = Auth::user();
    return view('frontend.pages.users.reviews', compact('user'));
  }

  public function store(Request $request)
  {

    $this->validate($request, [
      'rating'  => 'required|numeric',
      'message'  => 'required',
      'product_id'  => 'required'
    ],
    [
      'message.required'  => 'Please give a message for your review',
      'rating.required'  => 'Please give a rating for your review',
    ]);

    $review = Review::where('user_id', Auth::id())->where('product_id', $request->product_id)->first();
    if (is_null($review)) {
      $review = new Review();
      $message = 'Your review has added successfully !!';
    }else {
      $message = "You've already given a review for this product. However, your review has changed to this.";
    }
    $review->rating = $request->rating;
    $review->message = $request->message;
    $review->user_id = Auth::id();
    $review->product_id = $request->product_id;
    $review->status = 1;

    $review->save();
    session()->flash('success', $message);
    return back();
  }

  public function update(Request $request, $id)
  {
    $review = Review::find($id);

    $this->validate($request, [
      'rating'  => 'required|numeric',
      'message'  => 'required',
      'product_id'  => 'required'
    ],
    [
      'message.required'  => 'Please give a message for your review',
      'rating.required'  => 'Please give a rating for your review',
    ]);

    $review->rating = $request->rating;
    $review->message = $request->message;
    $review->user_id = Auth::id();
    $review->product_id = $request->product_id;
    $review->status = 1;

    $review->save();
    session()->flash('success', 'Your review has updated successfully !!');
    return back();
  }

  public function delete($id)
  {
    $review = Review::find($id);
    if ($review) {
      $review->delete();
      session()->flash('success', "Review has deleted !!");
    }else {
      session()->flash('sticky_error', "There is no review by this ID !!");
    }
    return back();
  }

}
