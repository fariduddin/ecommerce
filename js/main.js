jQuery(document).ready(function(){


	//Hover over navigation bar active
	$(".dropdown").hover(
		function() {
			$('.dropdown-menu', this).show();
		},
		function() {
			$('.dropdown-menu', this).hide();
		});


		/**Scroll and active go to top**/

		//Check to see if the window is top if not then display button
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('.scroll-to-top').removeClass('animated slideOutRight hidden');
				$('.scroll-to-top').addClass('animated slideInRight');
				$('.scroll-to-top').fadeIn();
			} else {
				$('.scroll-to-top').addClass('animated slideOutRight');
				$('.scroll-to-top').fadeOut();
			}
		});

		//Click event to scroll to top
		$('.scroll-to-top').click(function () {
			$('html, body').animate({scrollTop: 0}, 800);
			return false;
		});
		/**Scroll and active go to top**/



		/** Sticky Navigation bar On scroll **/
		var navOffset = $(".header-navigation").offset().top;
		$(window).scroll(function () {
			var scrollPos = $(window).scrollTop();
			if (scrollPos >= navOffset) {
				$(".header-navigation").addClass("navbar-fixed, navbar-fixed-top");
				// $(".header-cart").addClass('block')
				$(".header-navigation").addClass("sticky-top");
			} else {
				$(".header-navigation").removeClass("navbar-fixed, navbar-fixed-top");
			}

		});
		/** Sticky Navigation bar On scroll **/




	/** Start Jquery UI Slider for product **/
        var getOutput = $("#state");
        var getSlider = $("#slider");
        getSlider.slider({
			range: true,
            step: 10,
            min: 0,
			max: 5000,
			values: [0, 1000],
			slide: function(event, ui){
				getOutput.html(ui.values[0] + '-' + ui.values[1] + ' Taka');
				$("#minPrice").val(getSlider.slider('values', 0));
				$("#maxPrice").val(getSlider.slider('values', 1));
			}
        });
        getOutput.html(getSlider.slider('values', 0) + '-' + getSlider.slider('values', 1) + ' Taka');
        /** End Jquery UI Slider for product **/





		//Start owl carousel
		//$(".owl-carousel").owlCarousel();
		var owl = $('.owl-carousel');
		var slidesPerPage = 4;
		owl.owlCarousel({
			animateOut: 'lightSpeedOut',
			animateIn: 'lightSpeedIn',
			items:slidesPerPage,
			loop:true,
			margin:10,
			autoplay:true,
			autoplayTimeout:1500,
			autoplayHoverPause:true,
			// nav    : true,
			smartSpeed :900,
			// navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"]
			responsive:{
				0:{
					items:1
				},
				600:{
					items:3
				},
				1000:{
					items:4
				}
			}
		});
		$('.play').on('click',function(){
			owl.trigger('play.owl.autoplay',[1000])
		})
		$('.stop').on('click',function(){
			owl.trigger('stop.owl.autoplay')
		})

		$(".owl-prev").click(function () {
			owl.trigger('prev.owl.carousel');
		});

		$(".owl-next").click(function () {
			owl.trigger('next.owl.carousel');
		});


		//Initialize wow()
		new WOW().init();

	});
