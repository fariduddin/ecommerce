//Javascript bootstrap form validation
(function() {
  "use strict";
  window.addEventListener("load", function() {
    var form = document.getElementById("needs-validation");
    if (form) {
      form.addEventListener("submit", function(event) {
        if (form.checkValidity() == false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add("was-validated");
      }, false);
    }

  }, false);

  //For dataTable
  $('#dataTable').dataTable( {
    "lengthMenu": [50, 100, "All"]
  } );
}());
