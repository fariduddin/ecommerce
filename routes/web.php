<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'Frontend\PagesController@index')->name('index');
Route::get('product', 'Frontend\ProductController@index')->name('product');
Route::get('cart', 'Frontend\CartController@index')->name('product');
Route::get('checkout', 'Frontend\CheckoutController@index')->name('product');
Route::get('checkoutReview', 'Frontend\CheckoutReviewController@index')->name('product');
//Backend

Route::group(['prefix' => 'frontend'], function () {
    Route::get('product', 'Frontend\ProductController@index')->name('product');

});
Route::group(['prefix' => 'products'], function(){
    Route::get('/', 'Frontend\ProductsController@index')->name('products.index');
    Route::get('/latest', 'Frontend\ProductsController@latest')->name('products.latest');
    Route::get('/{slug}', 'Frontend\ProductsController@show')->name('products.show');
    Route::get('/search/q/', 'Frontend\ProductsController@search')->name('products.search');

    Route::get('/search/filter/price-range', 'Frontend\ProductsController@price_fliter_get')->name('product.price_filter_get');
    Route::post('/search/filter/price-range', 'Frontend\ProductsController@price_fliter_post')->name('product.search.filter.price_range');

    Route::post('/search/filter', 'Frontend\ProductsController@filter_product')->name('product.search.filter_product');
  });







/** End Front Page Routes **/



/** Admin Panel Routes **/
/**
* Admin Routes
*/
Route::group(['prefix' => 'admin'], function(){
    // Admin Login
    Route::get('/login', 'Auth\Admin\LoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\Admin\LoginController@login')->name('admin.login.submit');
    Route::post('/logout', 'Auth\Admin\LoginController@logout')->name('admin.logout');



    // Admin Reset Password
    Route::get('/password/reset', 'Auth\Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/email', 'Auth\Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset/{token}', 'Auth\Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::post('/password/reset', 'Auth\Admin\ResetPasswordController@reset');


    // Admin Change Password
    Route::get('/', 'Backend\PagesController@index')->name('admin.dashboard');
    Route::get('/change-password', 'Backend\PagesController@changePasswordForm')->name('admin.changePasswordForm');
    Route::post('/change-password', 'Backend\PagesController@changePassword')->name('admin.changePassword');




    /**
    * Manage Admin Creating + Editing and other routes
    */
    Route::group(['prefix' => 'manages'], function(){
      Route::get('/', 'Backend\AdminsController@index')->name('admin.getAdmins');

      Route::get('/my-profile', 'Backend\AdminsController@myProfile')->name('admin.myProfile');
      Route::post('/my-profile/update', 'Backend\AdminsController@myProfileUpdate')->name('admin.myProfileUpdate');

      Route::get('/create', 'Backend\AdminsController@create')->name('admin.create');
      Route::post('/store', 'Backend\AdminsController@store')->name('admin.store');
      Route::get('/edit/{id}', 'Backend\AdminsController@edit')->name('admin.edit');
      Route::post('/update/{id}', 'Backend\AdminsController@update')->name('admin.update');
      Route::post('/delete/{id}', 'Backend\AdminsController@destroy')->name('admin.delete');
    });




    // Product Routes
    Route::group(['prefix' => '/products'], function(){
      Route::get('/', 'Backend\ProductsController@index')->name('admin.products');
      Route::get('/create', 'Backend\ProductsController@create')->name('admin.product.create');
      Route::get('/edit/{id}', 'Backend\ProductsController@edit')->name('admin.product.edit');

      Route::post('/store', 'Backend\ProductsController@store')->name('admin.product.store');

      Route::post('/product/edit/{id}', 'Backend\ProductsController@update')->name('admin.product.update');
      Route::post('/product/delete/{id}', 'Backend\ProductsController@delete')->name('admin.product.delete');
      Route::post('/product/delete/image/{id}', 'Backend\ProductsController@deleteImage')->name('admin.product.image.delete');
      Route::post('/product/add/featured/{id}', 'Backend\ProductsController@addFeatured')->name('admin.product.addFeatured');
    });


    // Order Routes
    Route::group(['prefix' => 'orders'], function(){
      Route::get('/', 'Backend\OrderController@index')->name('admin.orders.index');
      Route::get('/all', 'Backend\OrderController@all')->name('admin.orders.all');
      Route::get('/seen-orders', 'Backend\OrderController@seen')->name('admin.orders.seen');
      Route::get('/completed-orders', 'Backend\OrderController@completed')->name('admin.orders.completed');
      Route::get('/view/{id}', 'Backend\OrderController@show')->name('admin.orders.show');
      Route::post('/make-seen/{id}', 'Backend\OrderController@toggleSeen')->name('admin.orders.make_seen');
      Route::post('/make-paid/{id}', 'Backend\OrderController@makePaid')->name('admin.orders.make_paid');
      Route::post('/make-completed/{id}', 'Backend\OrderController@makeCompleted')->name('admin.orders.make_completed');
      Route::post('/delete/{id}', 'Backend\OrderController@delete')->name('admin.orders.delete');
    });


    // User Routes
    Route::group(['prefix' => 'users'], function(){
      Route::get('/', 'Backend\UserController@index')->name('admin.users.index');
      Route::get('/view/{id}', 'Backend\UserController@show')->name('admin.users.show');
    });


    // User Routes
    Route::group(['prefix' => 'careers'], function(){
      Route::get('/', 'Backend\CareersController@index')->name('admin.careers.index');
      Route::post('/delete/{id}', 'Backend\CareersController@destroy')->name('admin.careers.delete');
    });


    // Category Routes
    Route::group(['prefix' => '/categories'], function(){
      Route::get('/', 'Backend\CategoriesController@index')->name('admin.categories');
      Route::get('/create', 'Backend\CategoriesController@create')->name('admin.category.create');
      Route::get('/edit/{id}', 'Backend\CategoriesController@edit')->name('admin.category.edit');

      Route::post('/store', 'Backend\CategoriesController@store')->name('admin.category.store');

      Route::post('/category/edit/{id}', 'Backend\CategoriesController@update')->name('admin.category.update');
      Route::post('/category/delete/{id}', 'Backend\CategoriesController@delete')->name('admin.category.delete');
    });

    // Navigation Menu Routes
    Route::group(['prefix' => '/navigation_menus'], function(){
      Route::get('/', 'Backend\NavigationMenusController@index')->name('admin.navigation_menus');
      Route::get('/create', 'Backend\NavigationMenusController@create')->name('admin.navigation_menu.create');
      Route::get('/edit/{id}', 'Backend\NavigationMenusController@edit')->name('admin.navigation_menu.edit');

      Route::post('/store', 'Backend\NavigationMenusController@store')->name('admin.navigation_menu.store');

      Route::post('/navigation_menu/edit/{id}', 'Backend\NavigationMenusController@update')->name('admin.navigation_menu.update');
      Route::post('/navigation_menu/delete/{id}', 'Backend\NavigationMenusController@delete')->name('admin.navigation_menu.delete');
    });

    // Brand Routes
    Route::group(['prefix' => '/brands'], function(){
      Route::get('/', 'Backend\BrandsController@index')->name('admin.brands');
      Route::get('/create', 'Backend\BrandsController@create')->name('admin.brand.create');
      Route::get('/edit/{id}', 'Backend\BrandsController@edit')->name('admin.brand.edit');

      Route::post('/store', 'Backend\BrandsController@store')->name('admin.brand.store');

      Route::post('/brand/edit/{id}','Backend\BrandsController@update')->name('admin.brand.update');
      Route::post('/brand/delete/{id}','Backend\BrandsController@delete')->name('admin.brand.delete');
    });

    // Payment Routes
    Route::group(['prefix' => '/payments'], function(){
      Route::get('/', 'Backend\PaymentsController@index')->name('admin.payments');
      Route::get('/create', 'Backend\PaymentsController@create')->name('admin.payment.create');
      Route::get('/edit/{id}', 'Backend\PaymentsController@edit')->name('admin.payment.edit');

      Route::post('/store', 'Backend\PaymentsController@store')->name('admin.payment.store');

      Route::post('/payment/edit/{id}', 'Backend\PaymentsController@update')->name('admin.payment.update');
      Route::post('/payment/delete/{id}', 'Backend\PaymentsController@delete')->name('admin.payment.delete');
    });

    // Color Routes
    Route::group(['prefix' => '/colors'], function(){
      Route::get('/', 'Backend\ColorsController@index')->name('admin.colors');
      Route::get('/create', 'Backend\ColorsController@create')->name('admin.color.create');
      Route::get('/edit/{id}', 'Backend\ColorsController@edit')->name('admin.color.edit');

      Route::post('/store', 'Backend\ColorsController@store')->name('admin.color.store');

      Route::post('/color/edit/{id}', 'Backend\ColorsController@update')->name('admin.color.update');
      Route::post('/color/delete/{id}', 'Backend\ColorsController@delete')->name('admin.color.delete');
    });

    // Pages Routes
    Route::group(['prefix' => '/pages'], function(){
      Route::get('/', 'Backend\SitePagesController@index')->name('admin.pages');
      Route::get('/create', 'Backend\SitePagesController@create')->name('admin.page.create');
      Route::get('/edit/{id}', 'Backend\SitePagesController@edit')->name('admin.page.edit');

      Route::post('/store', 'Backend\SitePagesController@store')->name('admin.page.store');

      Route::post('/page/edit/{id}', 'Backend\SitePagesController@update')->name('admin.page.update');
      Route::post('/page/delete/{id}', 'Backend\SitePagesController@delete')->name('admin.page.delete');
    });

    // Division Routes
    Route::group(['prefix' => '/divisions'], function(){
      Route::get('/', 'Backend\DivisionsController@index')->name('admin.divisions');
      Route::get('/create', 'Backend\DivisionsController@create')->name('admin.division.create');
      Route::get('/edit/{id}', 'Backend\DivisionsController@edit')->name('admin.division.edit');

      Route::post('/store', 'Backend\DivisionsController@store')->name('admin.division.store');

      Route::post('/division/edit/{id}', 'Backend\DivisionsController@update')->name('admin.division.update');
      Route::post('/division/delete/{id}', 'Backend\DivisionsController@delete')->name('admin.division.delete');
    });

    // District Routes
    Route::group(['prefix' => '/districts'], function(){
      Route::get('/', 'Backend\DistrictsController@index')->name('admin.districts');
      Route::get('/create', 'Backend\DistrictsController@create')->name('admin.district.create');
      Route::get('/edit/{id}', 'Backend\DistrictsController@edit')->name('admin.district.edit');

      Route::post('/store', 'Backend\DistrictsController@store')->name('admin.district.store');

      Route::post('/district/edit/{id}', 'Backend\DistrictsController@update')->name('admin.district.update');
      Route::post('/district/delete/{id}', 'Backend\DistrictsController@delete')->name('admin.district.delete');
    });





    // Settings
    Route::group(['prefix' => 'settings'], function(){
      Route::get('/', 'Backend\SettingController@index')->name('admin.settings.index');
      Route::post('/update', 'Backend\SettingController@update')->name('admin.settings.update');
      // Route::get('/delete', 'Backend\SettingsController@delete')->name('admin.settings.delete');
    });


    // Slider
    Route::group(['prefix' => 'sliders'], function(){
      Route::get('/', 'Backend\SliderController@index')->name('admin.sliders.index');
      Route::post('/create', 'Backend\SliderController@store')->name('admin.sliders.store');
      Route::post('/edit/{id}', 'Backend\SliderController@update')->name('admin.sliders.update');
      Route::post('/delete/{id}', 'Backend\SliderController@destroy')->name('admin.sliders.delete');
    });


    // Reviews
    Route::group(['prefix' => 'reviews'], function(){
      Route::get('/', 'Backend\ReviewController@index')->name('admin.reviews.index');
      Route::post('/change-status/{id}', 'Backend\ReviewController@change_status')->name('admin.reviews.change_status');
      Route::post('/delete/{id}', 'Backend\ReviewController@delete')->name('admin.reviews.delete');
    });




    // Contact
    Route::group(['prefix' => 'contact'], function(){
      Route::get('/', 'Backend\ContactController@index')->name('admin.contact.index');
      Route::get('/message/{id}', 'Backend\ContactController@show')->name('admin.contact.show');
      Route::post('/delete/{id}', 'Backend\ContactController@destroy')->name('admin.contact.delete');
    });

  });
  /** End Admin Panel Routes **/

  Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
